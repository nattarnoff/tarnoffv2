---
layout: page
title: "Portfolio"
date: 2013-04-07 09:31
comments: false
sharing: true
footer: false
class: portfolio
---

Much of my work over the years is proprietary and under contract in a manner
that I am not able to display it. Here are a few select items I can show.

## Internal People Management Solution

I worked on providing a base design solution for a personnel management solution
that would last the revamping of the agency's internal software platforms over
the next 4 years. The project included research, testing, training, wireframes,
design, living styleguide, and HTML prototype.

![PMA Dashboard](/img/pma.png)

[Demo link](http://mc.tarnoff.info)

## Sargento Achievments

Role: Ideation, wireframe, code execution

![Sargento Our Achievements](/img/sargento.png)

[Sargento.com](https://www.sargento.com/our-story/our-achievements)

## Four Letter Words

Personal project used to experiment on various frameworks including iOS,
Cordova, and Angular

![Four Letr Wrds launch Screen](/img/4ltrwrds-8.jpg){: .small }
![Four Letr Wrds Play Screen](/img/4ltrwrds-1.jpg){: .small }
![Four Letr Wrds error Screen](/img/4ltrwrds-4.jpg){: .small }


## Safe Driver Portal

Design for a usage based insurance program showing the user where they can
improve their driving and thus get bette rinsurance rates.

![Driver Dashboard 3](/img/driver-dashboard-3.jpg)
![Driver Dashboard 1](/img/driver-dashboard-1.jpg){: .small }
![Driver Dashboard 2](/img/driver-dashboard-2.jpg){: .small }

## Smaller Projects & Experiments

### [Redacted.js](http://gtarnoff.github.io/redactedjs/)

I've been in presentations where peopl ehave used foul language in their
screenshots by accident. This is a tool to black out those words similar to the
way government agencies redact classified information.

### [Fleggo](http://gtarnoff.github.io/Fleggo/)

I was fed up using grids that place the system in the HTML, so I began
developing a system of my own based on Flexbox tht has it contained in the CSS.


