(function($,doc,win){
  "use strict";
  var apiURL = 'https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=8afebf2f12e0935e4d1e9ed3611cb027&photoset_id=72157621180754354&format=json';
  var i = 0, obj;
  function buildImg(){
    var cap = i+9;
    console.log(i, cap, obj.length);
    if(cap <= obj.length){
      for(i; i < cap; i++) {
        var photoID = obj[i].id,
            photoTitle = obj[i].title,
            farm = obj[i].farm,
            server = obj[i].server,
            secret = obj[i].secret,
            html = '<figure class="photo__holder">';
            html += '<img class="photo" src="'+'https://farm'+farm+'.staticflickr.com/'+server+'/'+photoID+'_'+secret+'_n.jpg'+'" alt="'+photoTitle+'">';
            html += '<figcaption class="photo__caption">'+photoTitle+'</figcaption>';
            html += '</figure>';
            $('#photos').append(html);
      };
    } else {
      $('#loadMore--photos').addClass('is-disabled').text('All Photos Loaded')
    };
  };
  function getPhotos(i){
    $.ajax({
      url: apiURL
      ,context: doc.body
      ,dataType: 'jsonp'
      ,crossDomain: true
      ,jsonp: false
      ,jsonpCallback: "jsonFlickrApi"
    }).done(function(rsp){
      obj = rsp.photoset.photo;
      buildImg(i);    
    });
  };
  $(doc).ready(function(){
    getPhotos();
    $('#loadMore--photos').on('click', function(e){
      e.preventDefault();
      console.log('load more')
      buildImg();
    });
  });
}(jQuery, document, window));
