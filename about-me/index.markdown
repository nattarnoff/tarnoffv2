---
layout: page
title: "About Me"
date: 2013-04-07 09:31
comments: false
sharing: true
footer: false
class:  about
---
<img src="/img/Scotland-JS-104-web.jpg" alt="On stage at ScotlandJS" class="right circle" />

__Father. Buddhist. Web Developer. User Experience Professional. Accessibility Advocate. Community Organizer. Photographer. Yoga Lover. Movie Watcher. Equal Rights Advocate. Tinkerer. Logician.__

<blockquote class="pullquote"><p>I do that which I don&rsquo;t know how to do, so that I may know how to do it. -Picasso</p></blockquote>
<img src="/img/Scotland-JS-127-web.jpg" alt="On stage at ScotlandJS" class="left circle" />

I'm a front end developer by day and accessibility hero all the time. I have been building websites and applications for over a decade. From the very beginning I focused on building products that work for all users. Recently I began having difficulty of my own with vertigo. It has transformed how I see the world around me and how I build products with my team and clients. Additionally, I like long walks at sunset, photography, hand lettering, drawing and have a rabid obsession with single malt scotch.
The desire to understand how things work is one of my most driving factors. It has pushed me to take things apart in order to figure out how to put them together. Of course this led to me trying to make them do things they weren't supposed to do, otherwise known as hacking. 

