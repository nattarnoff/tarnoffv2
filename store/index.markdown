---
layout: page
title: "store"
date: 2013-04-07 13:17
comments: false
sharing: true
footer: false
---
##Available Products

###Stickers

<div class="product">
They're in! I just received 1000 of my stickers. If you have been trying to get one, you can order them here (otherwise you have to find me when I have stock). They come in packs of 5 for $5 with $2.00 shipping.
<img src="/img/logo12.png" alt="'That Guy' Sticker -- 5 Pack" />
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick">
  <input type="hidden" name="hosted_button_id" value="W6EHTTVS5BQX8">
  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form>
</div>
