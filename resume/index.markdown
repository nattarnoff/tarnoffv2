---
layout: page
title: "Resume"
date: 2013-04-07 09:31
comments: false
sharing: true
footer: false
class: resume
---

##Experience

###User Experience &amp; Accessibility

- Consulted on accessibility and user experience of company sites and
  applications from startups to Fortune 500, domestic and international
- Conducted A/B testing of designs with sample users
- Designed, prototyped and wrote coding standards for full ADA Section 508 and WCAG 2.0 AA compliant websites and applications
- Analyzed traffic patterns and abandonment rates of ecommerce solutions

###Web Development

- Hand code standards compliant, cross-browser HTML/HTML5/XHMTL &amp; CSS/CSS3
- Write JavaScript (jQuery, Dojo, YUI, MooTools, Prototype), XML, ASP, and PHP
- Designed, developed and deployed over 250 websites on multiple content management systems including ecommerce and Wordpress solutions
- Created design mock-ups and prototypes for use in functional documents, sales demonstrations, and by development teams
- Independently developed and deployed an application that provides a National Weather Service feed to the client websites

###Graphic Design

- Created print ready graphics for tradeshow signs, direct mail campaigns, and advertising posters
- Created icons for stand-alone applications
- Designed graphics and wrote HTML for email marketing campaigns

###Project Management

- Managed workflow and training for five member implementation team
- Created project plan, functional document, style guide and timelines for converting an
existing application’s pages to a new user interface
- Researched and wrote functional documents for future product development to expand
usability, shopping experience and to enter new markets
- Researched and wrote functional documents for internal and external multi-platform mobile applications
- Coordinate and run texting campaigns for a Fortune 500 company

###Search Engine Optimization

- Developed method to allow the editing of page/site meta data from one file without needing to access every page in the site
- Researched, wrote and implemented recommendations for increasing traffic to client websites through content, meta data and external linking
- Guided developers implementing changes to CMS that increased organic search engine listings
- Proposed new revenue streams from website maintenance and Pay Per Click advertising management
- Coached site owners to maximize the best returns in organic search listings

###Mobile Development

- Analyzed target markets to determine best mobile approach
- Managed projects to build multi-platform mobile solutions
- Educated Fortune 500 marketing department to build HTML5/CSS3 mobile compliant websites
- Devised and implemented strategy for future website development to take a mobile first approach
- Developing games for iOs devices
- Oversee all customer-facing mobile initiatives for Fortune 500 company

##Employment

###2015 to 2016 User Experience & Accessibility Advisor, Flexion, Sun Prairie, WI
- Work with advising teams on implementation of accessible UIs
- Design & develop front end solutions for studio clients
- Training internal & client teams
- Speaking and developer outreach

###2013 to 2015 Front End Engineer, Recurly, Inc, San Francisco, CA
- Work with the UX team to implement designs on the marketing site & application
- Established AB testing program
- Worked with marketing team to increase our placement in search engine results
  to first place in our keywords
- Developed Marketing campaigns

###2011 to 2014 President and Founder, Rolled Up Sleeves, LLC, Madison, WI
- Established LLC in 2011 for the development of mobile applications, web design and consulting

###2012 to 2013 Interactive Media Administrator, American Family Insurance
- Technical lead for all customer-facing websites and mobile applications

###2010 to 2012 Interactive Media Specialist, American Family Insurance, Madison, WI
- Lead developer for all customer-facing websites and mobile applications- Project Manage customer-facing and agent-facing mobile applications and initiatives- Core member of the agent mobility team deciding and implementing mobile tools and technologies for the field force- Core member for the Doctype Expert Group researching and deciding on company wide standards for HTML doctypes- Wrote templates, boilerplates, and standards for the coding of HTML5, CSS3, and JavaScript for upcoming site overhaul

###2009 to 2010 Front End Developer, Aquent contracted to American Family Insurance, Madison, WI
- Integral member of the Digital Marketing Team developing American Family Insurance’s
newly redesigned corporate website, microsites and applications including:
- Developed graphic elements for Coverage Calculator and Auto Estimate and Quote
- Participated on the Usability Standards team, defining standards for applications and
corporate websit
- Worked with the Search Engine Optimization team to increase visibility on the web

###2004 to 2010 Freelance Web Developer and Photographer, www.tarnoff.info, Madison, WI 

- Independent consultant for web development, design, social media, social media marketing, search engine optimization and professional photography to small and medium sized businesses

###2009 Front End Developer, Robert Half Technology contracted to Whittman-Hart, Brookfield, WI

- Created HTML, CSS and Javascript for client Harley-Davidson including: Rider’s Edge
class search pages, search engine optimized home page and product pages, motorcycle comparison pages, and sidecar product pages

###2007 to 2009 User Interface Designer, Connecture, Inc., Waukesha, WI
- Responsible for the conversion of a market leading software solution for health
insurance to a modern, standards compliant, fully accessible user focused design

###2005 to 2007 Web Designer/Implementation Specialist, ARI Network Services, Milwaukee, WI 
- Led sales, design, development and customer support teams in building over 250 new
websites; provided graphic design for tradeshow, direct mail, and email marketing

##Community Involvement

- An organizer of BarCamp Madison 3 (2010): An annual regional conference on technology, hacking, learning and civic leadership
- Founder of MadHackerHaus: A quarterly meeting for people creating new products, projects, and changes in their community
- Build Madison (2010): 24-hour hack-a-thon. Participated in building an iPad app to give those without voices a way to talk
- Web Designer, Milwaukee Shambhala Center (2008 - 2009)

##Public Speaking

I've been ramping up the number of speaking engaements lately. Instead of
maintaining them in two places, check out my [public speaking](/speaking/) page.

##Education

- Techskills, LLC, Brookfield, WI: Multiple certifications
- Pratt Institute, New York City, NY: Computer Graphics
- University of Wisconsin Milwaukee, Milwaukee, WI: BA in Philosophy
- Madison College, Madison, WI: iPhone Development Certification


