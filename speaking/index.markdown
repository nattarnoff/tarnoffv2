---
layout: page
title: "Public Speaking"
date: 2013-04-07 09:31
comments: false
sharing: true
footer: false
class: speaking
---

<img src="/img/onstage.jpg" alt="Speaking at MidwestUX in 2014"  class="right circle" />

I'm active in the community presenting talks around design, accessibility,
empathy, and building better interfaces. Over the last several years I have
presented at nine major conferences as well as screencasts and podcasts. 

Because I believe in the community and in teaching others to help grow the
community and the things we do, I have primarily funded these ventures from my
own pocket. This isn't something that is maintainable in the long run. 

Please [contact](/contact/) me if you'd like me to give a presentation or be on
your show. If participating requires travel, please see my rider below.

## Rider

- Organizers will pay for travel to the event
- Organizers will pay for lodging while at the event 
- Lodging can be hotel or AirBNB (I choose the AirBNB)
- We can negotiate you paying upfront or reimbursing me

## Talks

### Designing With Empathy

<iframe width="100%" class="talks" src="https://www.youtube.com/embed/KtcM4l5qd4A" frameborder="0" allowfullscreen></iframe>

#### Presented at

- OSFeels, Seattle, WA, 2015

---

### Infinite Canvas, Screencast, 2015

<iframe width="100%" class="talks" src="https://www.youtube.com/embed/QhnIZh0xwk0" frameborder="0" allowfullscreen></iframe>

--- 

### CTRLClickCast, Podcast, 2015

[Episode 49: Accessibility 101](http://ctrlclickcast.com/episodes/accessibility-101)

---

### UX of Stairs: When Simple Things Aren't So Simple

<iframe  width="100%" class="talks" src="https://www.youtube.com/embed/TfiNP8iWDSo" frameborder="0" allowfullscreen></iframe>

#### Presented at

- CodePaLousa, Louisville, KY, 2015
- That Conference, Wisconsin Dells, WI, 2014
- Midwest UX, Indianapolis, IN, 2014

---

### Accessibility: Building Accessible Web Apps, 2014

<iframe width="100%" class="talks" src="https://www.youtube.com/embed/ZxyLSTcWcmU" frameborder="0" allowfullscreen></iframe>

#### Presented at
- Ruby Hangouts, 2014

---

## Archived Talks

### 2013 CodePaLousa, Louisville, KY 
- Building Great Web Experiences for All Users - Workshop
- Turning the Titanic: Getting Large Enterprise to think Mobile First

### 2012 Madison Ruby, Madison, WI 
- Building the Web for Everyone

### 2010 Madison Non-Profit Day, Madison, WI

- Non-Profits and the Accessible Web

### 2010 BarCamp Madison 3, Madison, WI

- Photo touch-up 101 with Photoshop
- Accessibility with Web and Mobile Technologies

### 2010 BarCamp Milwaukee 5, Milwaukee, WI

- Accessibility with Web and Mobile Technologies

### 2009 BarCamp Milwaukee 4, Milwaukee, WI

- Section 508 and You: Or How I Learned to Love the Alt Attribute
- When Vegans Attack: Health, Ecological and Humanitarian Reasons to Be Vegan

