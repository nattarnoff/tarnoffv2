---
layout: page
hero: '/img/clouds2.jpg'
class: connect
title: "Thank You"
---


## Rest assured I got your message

Since you took the time to email me, I'll reach out to as soon as I can.
(Besides, I really like Inbox 0). 

Regards,

Nat
