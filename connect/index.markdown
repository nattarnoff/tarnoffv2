---
layout: page
title: "Connect"
date: 2013-04-07 09:32
comments: false
sharing: true
footer: false
class: connect
---

<h2>Email</h2>

Hi. Thanks for reaching out. You can send me a message right from here, or reach
out on my social media channels. 

However, if you are filling out this form (or sending me any message) to
question or discuss my gender identity, sexual orientation, or other aspects of
my identity&mdash;please don't. These are things I discuss with only my closest
of friends.

If you’re messaging me to find a safe place to ask questions, seek answers, 
or share ideas and thoughts about your own identity, all are welcome.

<form class="contact" action="http://formspree.io/greg.tarnoff@me.com"
method="POST">
	<ul>
		<li class="row">
			<label for="name">Your Name</label>
      <input type="text" id="name" name="name" placeholder="What should I call you?" required>
      </div>
		</li>
		<li class="row">
			<label for="email">Email Address</label>
			<input type="email" name="_replyto" id="email" required >
		</li>
		<li class="row">
			<label for="subject">Subject</label>
			<input type="text" placeholder="What do you want to talk about?" required
      name="_subject" id="subject">
		</li>
		<li class="row">
			<label for="message">Message</label>
			<textarea id="message" name="message" cols="30" rows="10"></textarea>
		</li>
		<li class="row">
			<button class="button">Submit</button>
      <input type="text" name="_gotcha" style="display:none" />
      <input type="hidden" name="_next" value="//tarnoff.info/connect/thanks" />
		</li>
	</ul>
</form>

