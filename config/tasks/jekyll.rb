namespace :jekyll do
  task :build, :roles => :web do
    run_locally "bundle exec jekyll build --config #{jekyll_config_file} --destination #{jekyll_build_path}"
  end

  task :remove_build, :roles => :web do
    run_locally "rm -rf #{jekyll_build_path}"
  end
end