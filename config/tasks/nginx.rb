namespace :nginx do
  desc " Restart the Nginx server"
  task :restart, :roles => :web, :except => { :no_release => true } do
    run "#{try_sudo} /etc/init.d/nginx restart"
  end
end