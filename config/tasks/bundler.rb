namespace :bundler do
  task :install, :roles => :web do
    run_locally "bundle install"
  end
end