desc "Deploy the Recurly Marketing site to the Staging Environment"
task :staging do
  set :jekyll_env, "staging"
  set :deploy_to, "/var/www/recurly/marketing/#{application}-beta"
end