desc "Deploy the Recurly Marketing site to the Production Environment"
task :production do
  set :jekyll_env, "production"
  set :deploy_to, "/var/www/recurly/marketing/#{application}"
end