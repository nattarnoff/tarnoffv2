require 'jekyll'

set :application, "www"
set :jekyll_build_path, "../_build"
set :jekyll_env, "staging"
set(:jekyll_config_file) { jekyll_env == "production" ? "_config.yml" : "_config-development.yml" }
set :repository,  "#{jekyll_build_path}"
set :scm, :none
set :deploy_via, :copy
set :copy_compression, :gzip
set :copy_exclude, ["Capfile", "/config"]
role :web, "54.83.16.126"
#"web-marketing1.ec2-east.recurly.net", "web-marketing2.ec2-east.recurly.net"

set :use_sudo, true

default_run_options[:pty] = true
ssh_options[:forward_agent] = true
ssh_options[:keys] = [File.join(ENV["HOME"], ".ssh", "id_rsa")]

# HOOKS
before "deploy:build", "bundler:install"
before "deploy:update_code", "jekyll:build"
after "deploy", "jekyll:remove_build"

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"