---
layout: page
title: "Services"
date: 2013-04-07 09:31
comments: false
sharing: true
footer: false
class:  services
---
<img src="/img/Scotland-JS-104-web.jpg" alt="On stage at ScotlandJS" class="right circle" />

<blockquote class="pullquote"><p>I do that which I don&rsquo;t know how to do, so that I may know how to do it. -Picasso</p></blockquote>

## Background

I’ve worked for the past decade developing accessible web and mobile interfaces 
for companies of all sizes, ensuring that your interfaces  comply with Section 
  508, American Disabilities Act, and WCAG 2.0 guidelines.

## Accessibility Assessment

My accessibility assessment will include a product analysis, review of
recommended guidelines, and suggestions for remediation using the assistive 
technology tools that are most prevelant on the market,
including JAWS, Voiceover, and NVDA. My assessment review commonly includes:

- Broken links
- Missing image information
- Improper form structure
- Improper heirarchy and document structure
- Color and contrast effectiveness

## Accessibility Remediation

Once the assessment is complete, I offer in-house remediation, remote
pair-programming, and/or ongoing training on accessibility best practices.

My experience includes working within MVC systems including Java, Python, Ruby, JavaScript, .NET, and PHP. 

## Front End Development

Front end development has been my preferred space from the very beginning of
my career. I can be brought on to supplement your existing team or kickoff a new project
assisting you tomore efficiently meet your timelines and goals, always with
accessibility as my foremost goal.

I craft strong HTML5, CSS, and JavaScript applications and websites 
using a variety of tools including: Sass, Less, and Stylus CSS Preprocessors;
HTML5 frameworks like Bootstrap, Material Design, and Foundation; JavaScript
libraries such as Angular, Node, React, Jquery, and Ember.

## Accessibility Training

Get your company booked on my training circuit &mdash; My curriculum can be 
brought directly into your facilities for a focused training. 
I offer in-depth presentations on accessibility, but can also tailor 
trainings to fit your specific business needs.


## Reach Out & Let's Work Together

Ready to discuss your project? Just send me an email:

<form class="contact" action="http://formspree.io/greg.tarnoff@me.com"
method="POST">
	<ul>
		<li class="row">
			<label for="name">Your Name</label>
      <input type="text" id="name" name="name" placeholder="What should I call you?" required>
      </div>
		</li>
		<li class="row">
			<label for="email">Email Address</label>
			<input type="email" name="_replyto" id="email" required >
			<input type="text" value="working together request" type="hidden" name="_subject" >
		</li>
		<li class="row">
			<label for="message">Message</label>
			<textarea id="message" name="message" cols="30" rows="10"></textarea>
		</li>
		<li class="row">
			<button class="button">Submit</button>
      <input type="text" name="_gotcha" style="display:none" />
      <input type="hidden" name="_next" value="//tarnoff.info/connect/thanks" />
		</li>
	</ul>
</form>

