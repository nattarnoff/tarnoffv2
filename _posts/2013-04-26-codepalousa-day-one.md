---
layout: post
title: "CodePaLOUsa: Day One"
date: 2013-04-26 08:05
comments: false
categories: [Presentations]
hero: "/img/louisville-sunrise.jpg"
---

What a fun day! There was a little confusion in the scheduleing, but everything worked out well. 

It does appear some people interpreted the schedule to have me presenting the same thing twice, once in the morning and once in the afternoon. I was under the impression that I was presenting once all day. As it turns out my content was just enough to fill up the morning and I was available and able to present it again in the afternoon. In total I had about thirty people attend my presentation Building Great Web Experiences For All. 

<!-- more -->

There have been a couple of other hiccups in the hotel booking, but the folks at [CodePaLOUsa](http://www.codepalousa.com/) and the Lousiville Marriott were extremely helpful in getting them all worked out. Additionally, the room I was in didn't have a very clear path to walk to the front. I could barely get through, but had I been in a wheelchair or needed another assistive device, I would not have been able to make to the podium.

I have met some great folks, like [Elle Waters](https://www.twitter.com/nethermind), who is part of the great company Simply Accessible and a Louisville local. She was able to provide us the lowdown on good vinyl record shops, trendy art areas and agreat vegan food (which all seemed to be closed last night) in the Louisville area. 

I talk again on Saturday about getting your clients to think mobile first. Hopefully you will be able to check out that talk as well.

While I sit here writing this post, the keynote is going on. The presenter is a lead engineer for a local SAAS company that is a sponsor. He just made a joke about the fact he has 3 daughters and the testosterone to estrogen ratio in his house is out of balance. There were a few chuckles, but I think the coffee hasn't kicked in yet. Looking around the room about 5% of the attendees are women. Most of the folks are white by a huge ratio. When I asked about diversity, I was told they are trying to get more Ruby, JavaScript speakers as they realize previous years was heavy in .Net and Java. We really need to work on this industry.
