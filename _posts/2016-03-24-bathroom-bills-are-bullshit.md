---
layout: post
date: 2016-03-24 10:48 
section: blog
categories: discrimination
title: "Bathroom Bills are Bullshit"
---

## Content Warning

*The following post contains some traumatic and triggering scenarios. There is
foul language and references to body parts.*

North Carolina just made it a law that transgender people will no longer be able to use bathrooms in state owned buildings. It’s only slightly more nuanced than that, but the reality is that implementing this law means that colleagues, friends, members of my family, and likely me\* will not be able to go to the bathroom when visiting anything from schools, to the capitol, to state parks. And this is bullshit.

<!-- more -->

Bathroom bills are written into law supposedly to protect the privacy of our children. Lawmakers think that because someone doesn’t conform to the idea that if they are born with a penis, they are a man, that these feelings also mean the person wants to molest children. And this is simply not the truth. The reality is that there is no federal law that prevents people from discriminating against transgender people in the workplace, housing, or really anywhere unlike the laws that exist to account for disabilities, creed, race, religion, or sexual orientation. There is a certain segment of the population, we’ll call them bullies, that revels in oppressing others. Now we still deal with institutionalized racism, sexism, ableism, and homophobia, but the federal government has seen fit to make it illegal when it is proven. Transgender people don’t yet have this protection. And that’s what bathroom bills are about.

## How do I know it isn’t about privacy? 

Because as a boy growing up, no one gave a shit about my privacy in bathrooms. In fact the violation of my privacy resulted in comments like, “boys will be boys,” “man up,” and “don’t be such a pussy.” In school, and I’ll focus on school because this is all about the children after all, it was common for boys to pick on each other in the bathrooms and the locker rooms.

One of the grade schools I attended removed the doors on the stalls because some boys were writing graffiti on them. Once this happened, none of the staff ever used the non-staff bathrooms. Before the age of eleven I was forced to drop drawers and squat on a toilet taking a dump in full view of half the school if they could cram into the bathroom.

Another school I’m familiar with only had half walls between stalls. Yes there were doors, but the walls were only high enough to hide you from your chest down. This was done to ensure that students weren’t doing drugs and teachers were instructed to check the bathrooms on a regular basis. The lower walls allowed them a quick glance. It also allowed anyone on either side of you to lean over and see…your stuff.

In middle school we had to change for gym class, including swimming. We were instructed to shower after class as well. We were growing boys after all and puberty makes you stinky. I was a husky boy. I wasn’t lean like my classmates. I started shaving in middle school, but alas, body hair wasn’t showing up yet. Despite what media tells us in high school comedies, having a beard, mustache, or chest hair did not in fact make you the “cool kid.” It made you a target. If you wore the wrong underwear, you were a target. If you were overweight, you were a target. If you didn’t shower, you were a target. If your towel had the wrong thing on it, you were a target. If you were shy, you were a target. Any reason  boys could ”other” you became a reason to belittle, towel snap, beat up, push, shove, or bully. And of course as we grew older and puberty took hold, then there were the real competitions.

Boys in locker rooms and bathrooms measured everything. Nothing got measure more than their dicks. Everybody is endowed differently. It’s a genetic thing. But it was an easy way to pick on people. The length of your penis showed how much of a man you were. And the entire time this happened, no one cared about our privacy.

Because of this, at the age of 41, I struggle to use public bathrooms and lockers rooms without clear, private, locking stalls. I was picked on so much for my body, underwear, hair, towel, shower regimen, desire for privacy, and size of my penis that I have lifelong PTSD I’m still coping with. Bathroom bills aren’t about privacy. They are about controlling one group of people that is different than you.

## We need your help

Right now we need our political leaders to speak out against this. We need our courts to uphold our rights. We need our siblings in the LGBT community to stand at our sides and fight for the rights of the silent T. You got your rights, please help us get ours. We need real visibility on this. Bernie Sanders, Hillary Clinton, President Obama, Elizabeth Warren, Harry Reid, Tim Cook, Jeff Bezos, Elon Musk, Mark Zuckerberg, Bill Gates, George Clooney, Tina Fey, Lena Dunham, Ellen Page, and anyone who cares about people please come out with public statements denouncing North Carolina and all other local, state, and federal attempts to keep transgender people second class citizens. And then help us make the following happen:

- Give us the same rights as all other Americans.
- Allow us to easily modify our birth certificates and legal records to match the gender we are.
- Allow us to have non-binary options on legal forms (not all of us identify as man or woman).
- Modify the building codes so there are also “All Gender” bathrooms in all public buildings as well as what is currently required.

### \*Why me?

In case you are new, here is the [link as to why me](http://tarnoff.info/blog/coming-out/). I'm very aware how the world sees me. I know that I have a lot of privilege and got to where I am on the privilege of the white cis het male facade I wore for 40 years. But I'm not. Most people will see me as a "cross dresser" and mentally assign me male because I wear feminine clothing and have a beard. They don't identify me as transgender despite my fitting the [definition](http://www.merriam-webster.com/dictionary/transgender). But I do get classified as weird, freaky, not right, and a laundry list of other things because I don't conform and will not to society's malformed rules regarding gender.
