---
layout: post
date: 2015-10-13 10:00
section: blog
title: "My Open Source & Feelings Talk is on Video Now"
---

If you couldn't make it to Open Source & Feelings, and I think a few people
missed it&mdash;don't make this mistake next year, I gave a talk on [Designing
with Empathy](/blog/designing-with-empathy/) which you can read or watch the video of below. Thanks to
Confreaks for taping this.

<!-- more -->

##Trigger Warning: Opening credits may cause nausea

<iframe width="560" height="315" src="https://www.youtube.com/embed/KtcM4l5qd4A" frameborder="0" allowfullscreen></iframe>
