---
layout: post
title: "How to Negotiate"
categories: [Work]
comments: work
comments: false
date: 2015-04-14 08:34
--- 

Negotiating is a tricky subject. Talking money seems to still be a taboo in
polite society. Subsequently, we often over pay for things or get underpaid for
the work we do. A friend asked about having a conference session on negotiating.
I began working on a talk, but I'm not sure it is ready yet. This article is an attempt
to get my thoughts out there so a discussion can be had to help me flesh out
the talk.

<!-- more -->

Why should I write about negotiation? I'm not sure I should, but I've had some
pretty good luck doing it. I've bought 2 cars at my asking price and one well
below the dealer's listed price. On several occasions I've negotiated
significant pay increases with my employer or raised my salary by taking a new job. 
Not everything I do will work for everyone, but I hope it helps you improve you
outcomes when you next negotiate.

# Know Your Role

The first thing we need to do is know which side of the negotiating table we are
standing on, because after all, we want to make sure we play our best hand
regardless of our role.

If you walk onto a car lot looking for a new vehicle, its pretty clear which
side you are on. You are the buyer. However, some people forget what side they
are on when it comes to negotiating a job. In this case, you are the seller.
**Your goods are the skills you bring to the job and the work you promise.** 

<p class="emphasis">The person with the money is always the buyer.</p>

The person with the money isn't always going to be the person with the power. In
a car negotiation, the dealer has the power. In a job negotiation, the employer
has the power. Every situation is different, but **you need to tip the scales of
power in your direction**.

## Plan for Negotiation

Do your research. Spend time before you head into the negotiation
getting to know the product, person or company you will be working with. If it
is a car, spend time on the auto websites learning the features and values on
the models you might like to buy. Look up the company on [GlassDoor](http://www.glassdoor.com) and other job
review sites to get a feel for what its like to work there. These are things
most folks think of, but there is more work to do.

Research the dealership. See if anyone has posted online
([Yelp](http://yelp.com) has reviews on dealerships) on their experience
negotiating and buying. If they mention a salesperson, look deeper into that
individual. They may be a good person to ask for or avoid when you head in. 

When applying for a job, research the position, but not just what the company posts.
Look at the details of the job and find other roles with other companies and
similar responsibilities. Namely, look to see the salary ranges they post. Check
out industry websites. [AIGA](http://designsalaries.aiga.org/) conducts an annual survey of the design industry 
calculate average salaries. Combine this with
your skill set to find a fair wage for your efforts.

At one point in my career, I realized I was doing a lot more work than the
company recognized with my salary. I did my research and discovered I was
actually doing the jobs of 5 people making at least what I was making if not far
more. It was definitely time to ask for a raise or find another job.

## Be Realistic

**The trick to negotiating is to make everyone feel like they got a great deal.** If
you can't make the other party feel like they did well in the deal, the deal
won't go through. This is why we prepare. 

My position where I wore five hats is a great example. The jobs I was doing
have salary ranges from $55,000 to $120,000. In theory I should be able to add
them all up and that should be my salary. But that isn't realistic. So I needed to
find a realistic number. 

## Goal Versus Base

Your base, or limit, is either the bottom as a seller or the top as a buyer for
your negotiation. **This is the point where if the number crosses it, you need to
get out of the discussion.** Otherwise you will be getting paid less, or spending
more than you can really afford. **Never cross your limits.**

**Your goal is your ideal number. The goal is what you get if your negotiation goes
perfectly.**

My base for that position was pretty easy to figure
out. My current salary paid my bills, allowed me to buy a home, and I was able
to save. But what was my value? I brought a lot to the table. To fill my shoes,
the company would need to spend over 5x what I was making. But asking for that
wasn't realistic. I knew what my boss made. I knew what his boss made. My salary
was already 85% of the range the company had for my role. I looked into what the
industry standard was in the area, as well as in major metro areas like New York
City, Chicago and San Francisco. It was realistic based on this research for me
to make 40% more than I was. 

Knowing I was severely underpaid and that my boss didn't make what I should be, I
looked for a new job with the goal of getting a 40% increase. Of course I could
take less if I wanted and the position was right.


<p class="emphasis">The goal is not your asking amount.</p>

This goal number is halfway between your limit and the
asking number. So how do we make sure our negotiation comes out in our favor? We
go in with the right number, one which the other party will try to talk us off
of but yet, will leave us with our goal.

## 20% Rule

This is not some sort of special secret sauce rule of great negotiators. This is
my rule. It works for me, it may not work for you. But it will provide you with
a how I think (which has worked out well for me) and allow you to structure your
negotiation to suit you.

If I am the buyer, once I know my limit, I cut it by 20%. This is the number I
use to start my negotiation with. Depending on how much I like the object I am
buying, I'll give on that 20% until I get it or hit my limit. **This gives me room
to work on the deal and make both parties feel good about it.**

If I'm the seller, I ask 20% higher than my goal number. 

When I decided to find that new job, I wanted to make that 40% increase. So 
**I asked for more money than the 40% increase.** I expected the new
employer to offer a lower amount. I expected that I would say that wasn't enough
and we would both alter our numbers to meet in the middle. I asked for a
number that was actually 73% higher than what I was currently making. This
wasn't the first time either. I previously had asked for a 50% increase over my
previous salary.

## Walk Away

If you are unwilling to walk away when you hit your limit, then you will not be
happy about the negotiation's outcome. In fact, **to make sure both parties are
happy, you need to be willing away to walk away sooner than your limit.**

I've walked on deals several times. One deal I actually walked four times. 
In all those cases, the other party came
back with another offer. An offer that I was able to accept and feel good about
it.

I know what you're thinking, **"I need this job. How can I walk away from a job offer that's less
than what I deserve?"** You've been working hard, or maybe you are unemployed, the
prospect of getting an offer in hand and walking away from it is pretty scary.
**But if you aren't willing to walk, you never had any power and didn't really
show up to negotiate.** So the best thing is to look for a job when you already
have one (this is easier said than done).

Before I started working for the five hat company, I actually walked away
from the offer. I had been working as a contractor with them for about nine
months. They wanted to bring me on full time. The offer they made was was 20%
below what I was making as a contractor (thank you overtime!). If I had not
accepted the offer, there was no guarantee of a job. They could cancel the
contract and I'd be unemployed. Still, I said no. The HR person was shocked. The
hiring manager was shocked. When I explained how much of a cut I was taking and
that the number they offered was low for the industry (with data to back that
up) they went back to see what they could do. 

I got an offer that was 5% higher and took the job.

## Don't Show Excitement

The more excitement you show, the more they know they hooked you. Keep calm,
work on your poker face, be cordial but unimpressed. To hold the power, you need
to get them to be more excited than you. I work hard to hold onto a facade of
malaise and disinterest in all my negotiations. **If you get excited, you lose the
power.**

## Dress for the Deal You Want

If you are interviewing for a job, dress one level better than them until you
hit the business suit mark. **Never wear a ball gown or tuxedo to an interview.** Of
course, this is also dependent on who is doing the pursuing. 

Most of the time I'll wear a jacket and tie to an interview. What kind of tie and whether I 
wear jeans or slacks is determined by the company I'm headed to. For the five
hat company I worked for, it was dress slacks & a conservative tie. For a
startup, it was jeans and a tie with skulls & crossbones on it. However, in a recent "interview" I
actually wore a flannel and jeans. The way that relationship began, I had all the power.
Showing up in a flannel demonstrated that I had the power. I was listening, but
I didn't need the job, so this was going to be on my terms. 

When I'm the buyer, I dress down. Way down. I get as frumpy and disheveled as I
can. This gives a false impression to the seller. One that throws off their
pricing model and when we can do that, we gain the upper hand.

## Practice

If you are new to negotiating, you will need to practice. Study up on your topic
and work with a friend or family member. You take your role and put your
helper on the other side. Ask them to play hardball. Remember, we want to
ruffle feathers, but maintain a calm experience. Talk it through, role play, and
do it until you feel confident you can do it in the real situation.

## Conclusion

You are probably wondering what happened during that negotiation for the new job
with the 73% increase in pay. I didn't get the deal I should have. 

At the time I was talking to three different companies. All of them I asked for
the same salary. One didn't work out because of my not being willing to
relocate. But I did get offers from the other two. I got great offers, both of
which would hit the goal I was seeking.

But one was better than the other. I took that job, but I ended up leaving money
on the table. I misjudged the market and my skills (I'm not perfect and this
isn't a science). When I asked for the larger amount, they paid it to me. They
didn't negotiate. I could have asked for more and maybe gotten it. I certainly
would have had a negotiation. I got extremely lucky, but then again I've had
lots of luck like this.

I once bought a car for 20% under my limit. I walked from the deal 4 times all
well holding firm to my asking price and
was even in the car driving off the lot when the salesperson came running up to
tell me I got the deal. 

Hopefully my tips and techniques will help you win that next negotiation.
