---
layout: post
title: "Verizon iPhone 5 LTE issues"
date: 2013-01-18 22:53
comments: true
categories: [Technology]
---

I am now on my second iPhone 5 and sim card from Verizon. I have been having issues with the time settings on my phone rolling back between 20 minutes and an hour (mostly 30 minutes). This prevented me from using some apps.

<!-- more -->

The only fix seemed to be restarting the phone. When it would reconnect with the network, the time would be adjusted and I could engage in timestamped activities like Twitter. At first turning on Airplane Mode didn't work (it does seem to today).

I ended up restoring my phone and starting clean. Still had issues. So I took the phone into Apple. Apple couldn't figure out what was wrong but replaced it this past Friday night. Between Friday 7pm and Sunday 4pm, my phone experienced the issue 5 times (previously it had been ~2 per week). I took it to Verizon for a new sim as this is the next controllable piece in the puzzle. This morning it locked up again.

Verizon didn't hesitate to replace the sim, but it took 45 minutes for it to be activated. During this time the store manager checked into the internal Verizon forums. He discovered 83 posts describing this problem in one thread as well as several similar threads. The only solution they offered was to turn off LTE as the issue is the way LTE is being handled with the phone. they expect a software fix by the end of the year.

I have now turned off LTE on my phone. I also expect Verizon to give me a discount for the time I will need to turn off LTE while I await this fix.

**UPDATE**: Spoke with Verizon. They confirmed it was a known issue and that Apple and Verizon techs were working on it, but they are refusing to offer a discount stating that "...you are paying for data, that is all data, not just LTE. That's a bonus."

**UPDATE 2**: After a couple more calls to Verizon, they identified the three towers that were giving me most of my trouble. They reset them with no improvement. I decided to forgo "automatically set time" from the network for a few days. Then I turned it back on. I haven't had any issues for a week now.
