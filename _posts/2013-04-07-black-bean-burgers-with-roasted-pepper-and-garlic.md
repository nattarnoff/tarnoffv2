---
layout: post
title: "Black Bean Burgers with Roasted Pepper and Garlic"
date: 2013-04-07 13:27
comments: false
categories: [Cooking]
---
Having to fed two omnivore children, especially when one leans to be more of a carivore, is always a challenge in a vegan house. One of the tricks I have found is to work with omnivorous meals and make them vegan. We have had the commercial burger patties in the past, but not all of those are actually vegan. I conceived this recipe as a variant of one my brother and sister-in-law make for their family. The flavor is off the charts, and when there are left overs, you can turn them into burritos instead of burgers.

<!-- more -->

##Ingredients##

- 2 cups cooked brown rice
- 1 can (15oz) or two cups cooked black beans
- 1 red pepper
- 1/2 medium sized head garlic (FYI: the whole garlic you buy at the store is a head, the indiviual pieces are the cloves)
- 2 Tbsp extra virgin olive oil
- 1/2 small yellow onion finely diced
- Salt and pepper to taste

Peel the garlic and put them into a glass cooking dish that has a lid. Mix the garlic with the olive oil making sure that each clove is well covered. Slice the top off the red pepper and place the rest of it over the garlic. Cover with the li and cook for 30 minutes at 400 degrees. Warm the black beans while this is cooking. Remove the beans after 15 minutes and mix with the rice. We want the mixture to be cool to the touch remember.

Once the garlic and pepper are roasted, mix them together in the food processor until you have a nice buttery mix. Stir this into you rice and bean mix with your finely diced onion. Start a fry pan on medium heat with some canola oil, enough to cover the bottom. When the oil is brought up to heat, make your patties. Try to make them about 1/4 inch thick and about 4 inches across. Much bigger than this and they will be hard to flip and will fall apart when eating.

Carefully place each patty into the fry pan and cook for about 5 minute per side. Place on your burger buns, garnish with your favorite condiments (relish is delish here) and enjoy. If you do have left overs and don’t want to cook up the patties with the oil, you can microwave the rice and bean mixture and put it into a tortilla for a great burrito.
