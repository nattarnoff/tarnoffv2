---
layout: post
date: 2015-08-13 14:00
section: blog
title: "Managing Chronic Illness While Working"
categories: health
---

Shortly after I developed my vestibular disorder, I began working remote, from home. Even before that, my employer accommodated me by letting me work from home if I ever had a really bad vertigo day. For the last two years, all my work has been remote, until this month. I’ve now figured out that I have a way of working that best meets my productivity and health needs and when I don’t get to work like that I get cranky, frustrated, and highly inefficient.

<!-- more -->

For the last five weeks I have been consulting on a project in Minnesota. This has required me traveling to the Twin Cities and staying in AirBNBs during the week and coming home on weekends to be with my kids. This particular client felt they needed me onsite to teach their team in the ways of front-end development and user experience in addition to providing a design solution. Not realizing I had a way of working that worked best for me, I gladly obliged. What I didn’t see coming is someone micromanaging everything I do and how that would make me feel.

When I work remote, I typically work 50 to 60 hours a week and it doesn’t bother me. In fact I enjoy it. But these hours are never worked in a typical “9 to 5” fashion. I try not to get up with an alarm, when I get a headache or vertigo I take a rest or a walk, I work in three or four hour sprints and do get through two or three of these a day. Some days I need to work six hours. Others I can work twelve or even sixteen hours. When I need to go see a doctor (which happens quite a bit) or take care of my family, I do it and put the hours in later. 

<blockquote class="pullquote">
they often ask me, “How do you do it every day?”
</blockquote>

During this contract though, my client has insisted that I work not only onsite, but 8 hours a day, 5 days a week. No more. I can’t work 10 hours if I’m in the zone. I can’t leave early or mid day and take a nap if I need one. I’ve also been asked to do 4 months worth of work in under two. If I could work my way, even while in the Twin Cities, I could probably accomplish this extreme goal. But since I’m limited to billing 40 hours a week, I keep having to adjust timelines and that makes me feel horrid. 

When I speak to people about my migraines and vertigo, they often ask me, “How do you do it every day?” I honestly don’t know. I’m the kind of person that if I do absolutely nothing all day, I feel horrible and unproductive. So despite having health issues that probably should leave me in bed, I get up and work. I do something. I don’t get a choice. In the past, before my vestibular disorder, I would work and work and work, eventually crashing because I didn’t maintain a healthy balance. Since I developed the vestibular disorder, I’ve learned to manage each moment and day a little better. Yes, there are days, weeks even, when I will over do it and I pay for it in the end. I'm left incapacitated for an entire day or several. I’ve spent weekends in bed because I didn’t manage my work load in a week.

And I can feel it coming now. I see the crash on the horizon and I don’t know how to stop it, because I’m in a contract I can’t terminate in which I’m being micromanaged and trying to fit my health needs around someone else’s idea of work, rather than fitting work around my health.

As managers, employers, and coworkers it is important that when someone with chronic illness is working with you, it is important to talk with them to find out what they need to be the best at their jobs. For some, it will be working from home on a slightly odd schedule. For others it will be having the right equipment in the office. But for all of us, if we have the right tools, environment, schedule, and understanding to work our way we will be reliable and produce quality work. When we are forced to prioritize work over taking care of ourselves, we will not achieve the goals we set out with you.
