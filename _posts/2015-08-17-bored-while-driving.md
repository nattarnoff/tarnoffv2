---
layout: post
date: 2015-08-17 10:00
section: blog
title: "Bored While Driving"
---

For the last several weeks I have been commuting from my home near Madison four
and half hours north to the city of Minneapolis. The only saving grace of all
this driving is the fact I have satellite radio and don't need to constantly
switch playlists or hunt down radio stations enroute to the Twin Cities. Last
 week was my last full week up there for this contract and on the ride home I 
 think I finally cracked.

  <!-- more -->

<img src="/img/car-selfie.jpg" alt="Self portrait taken while waiting out a rain
storm in my car" class="right" />

  Anyone who knows me, knows I love a good joke. More importantly, I love a bad
  joke. The punnier the better. The more awkward, the more I dig it. Well, I
  also love 80's music, mostly the alt, punk, and new wave, but even a little
  pop now and then. 

  While listening to SiriusXM's 1st Wave radio (80's alternative), I heard a few
  tracks that triggered an onslaught of missed branding opportunities by bands
  in the 80's based on either their name or the name of a song or album. 
  For posterity, here they are. I'm not sorry that I'm subjecting you to these.

  >Why hasn't Madonna released a line of juniors' maternity wear named "Papa Don't Preach"?

Ok, maybe I can see why that wouldn't have gone well in the 80's.

  >There should be a line of tie-dyed faux animal skins called "Psychedlic Furs".

Surely there was a market for these? Think of how many people wouldn't need to
have paint poured on their fur?

  >Devo could release a line of leather and latex lingerie called "Whip It".

I still think they can pull this one off now.

  >Michael Jackson should have released a line of denim called "Billie Jeans".

Alas, maybe Paris will do this when she gets older.

  >How about a haberdashery named "Men Without Hats".

Honestly, this one sounded better in my head.

  >U2 can release a line of shirts called "Joshua Tees".

  >Or perhaps a line of bedding "Where the Sheets Have No Names".

Should I stop? I didn't think so.

  >Robert Smith should open a chain of gothic pharmacies named "The Cure".

  >Bruce Springsteen should open an adoption agency, "Born in the USA".

Did I say that? Maybe that is pushing it.

  >Cyndi Lauper needs a line of hair dye named "True Colors".

  >The Red Hot Chili Peppers could release a line of post-maternity wear, "Mother's Milk".

  >"Faith" by George Michael could be a line of evangelical churches next door
  >to "I Want Your Sex," an escort business for the pastors.

Definitely pushing the envelope, but I couldn't help myself.

  >Concrete Blonde could have a pet store that only sells baby kangaroos, "Joey".

  >Men Without Hats release a line of equipment for ballerinas named "Safety Dance".

  >Tommy Tutone opens a chain of grafitti removal specialists called "Jenny", the number to call is 867-5309.

  >"Dancing in the Sheets" is a bedroom line by David Bowie and Mick Jagger.

  >Echo and the Bunnymen release artifical sweetener packets in the shapes of mouths, "Lips like Sugar".

Thank you for playing along. 
