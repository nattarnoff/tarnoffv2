---
layout: post
date: 2015-04-27 13:00
categories: accessibility
title: "CodePaLousa Accessibility Resources"
section: blog
---

Thanks to everyone who came out to my talk **UX of Stairs** this morning. It was
a really great crowd. there were a number of great questions and I mentioned
some really good tools and resources, but I didn't have the URLs on slides. Here
is a list mentioned in the talk as well as in some conversations I had
afterwards. **And if you missed my talk, you can catch a version of it [here](/blog/presentations/midwestux-ux-of-stairs/).**

<!-- more -->

## Blogs & Articles

- Simply Accessible: [examples](http://examples.simplyaccessible.com/),
  [articles](http://simplyaccessible.com/archives)
- [Paciello Group](http://paciellogroup.com)
- [A11y Project](http://a11yproject.com): my articles - 
  - [Video
  Captioning](http://a11yproject.com/posts/using-caption-services-with-html5-video/)
  - [Checking Reading Comprehension
    Level](http://a11yproject.com/posts/check-comprehension-level/)
  - [Understanding Vestibular
    Disorders](http://a11yproject.com/posts/understanding-vestibular-disorders/)


## Testing Tools

- [Tenon.io](http://tenon.io)
- [WAVE](http://wave.webaim.org)

## Examples

- [Animation Switch](http://codepen.io/gregtarnoff/pen/JoMxpK/)

## Additional Resources

- [Web Content Accessibility Guidelines 2.0](http://www.w3.org/TR/WCAG20/)
- [Do's and Don'ts for Web Accessibility](/blog/accessibility/dos-and-donts-for-web-accessibility/)
