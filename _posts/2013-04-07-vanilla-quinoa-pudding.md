---
layout: post
title: "Vanilla Quinoa Pudding"
date: 2013-04-07 13:24
comments: false
categories: [Cooking]
---
Found this on [Yahoo](http://shine.yahoo.com/event/green/how-to-make-quinoa-the-powerhouse-seed-2410950/).

Quinoa in this application results in a cross between rice pudding and tapioca, with more protein than either.

<!-- more -->

##Ingredients

- 3 cups whole milk (works well with soy or nut milk for a vegan version)
- 1 vanilla bean (split, or 1 tablespoon of vanilla extract)
- 1/4 cup maple syrup
pinch of salt
- 1 cup quinoa

Rinse quinoa. Mix milk, vanilla, maple syrup and salt in a saucepan and set on simmer. Add quinoa and cook for 30 minutes, stirring frequently (but not constantly).Once thickened, remove from heat and allow to cool. Serve warm, or refrigerate. Top with all kinds of delicious tidbits; berries, dried fruit, nuts, nutmeg, brown sugar, etc.
