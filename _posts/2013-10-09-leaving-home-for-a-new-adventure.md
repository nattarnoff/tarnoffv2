---
layout: post
title: "Leaving home for a new adventure"
date: 2013-10-09 21:49
comments: false 
categories: [Work]
---

A little over four years ago I was in a tough spot. I didn't have a job, my lease was up and I didn't know what I was going to do. I had burned through my savings, charged up my credit cards and prepaid on a storage garage for three months, using up the last of my money. I moved all my stuff into that garage or sold it and planned on moving into my parent's office at the age of 34.

<!-- more -->

Then I got a break. A couple of guys at American Family Insurance took a chance on me to do some contract work for a couple months. It still meant moving in with my parents, but I was going to be able to pay my bills. I helped complete a site redesign and impressed them enough that they extended my contract. 

So I rented an apartment and worked crazy hours just trying prove I had what it takes because I didn't want to go back to being unemployed. After 9 months of doing contract work, they offered me a job. 

I said no. 

They modified the offer and I accepted. I was part of corporate America and working in the financial sector. It was never a place I planned to be, but in 2010 I was glad to have a job.

For four years it has been a home for me. I was part of a rapidly growing digital marketing department and had the chance to work with a great pool of talented people that designed, wrote, developed websites and spread the word through social media. We shifted from grunts updating the corporate website to user experience professionals. The team became an internal "do all" digital agency. We took on supporting not just customer facing websites, but also mobile apps, digital advertising and even some internal projects. 

We experimented with edgy technology. We broke things. We hacked them back together. We focused on improving everything we did to the betterment of our customers' experience. I learned about A/B testing, user experience, how to build out a server, load balancing, performance tuning a site and a server, and took my programming skills to ever higher heights.

Some things we did aren't my best work, but most I'm really proud of. Most importantly, I'm proud of the team I was lucky to work with. I have made some good friends, but now it is time to say goodbye. 

## Fortuitious Circumstance

In August I began the strangest trip I've been on yet. I've worked hard over the last four years to be the best I can be. I have given talks at conferences, experimented with technology and made as many connections as I could in the industry. Apparently this paid off. I was approached by three companies to come work for them. 

I honestly didn't know how to handle it. In the past I had to fight for jobs, but now they were fighting for me. Of the three companies, two made me offers, neither were something I could turn down but I had to make a choice between staying or one of the companies. 

It was a hard choice, but I decided to walk away from the home and family I've had the last four years.

## The Next Adventure

Tomorrow is my last day at American Family. On Monday I start my new position as front end engineer for [Recurly](http://recurly.com). 

I'm lucky as I get to work from home and travel to San Francisco to work with the team frequently. It's the best of both worlds. 

It's an exciting move for me. Not only is something new, but it's my first time working for a startup. I'll get to do cutting edge stuff with HTML5, CSS3 and JavaScript. I'll be part of a team supporting a Ruby and Node application full time. It's all things I want to be doing and I'm crawling out of my skin waiting for my flight out there next week. 

It's like Christmas morning for me and I can't get any sleep.
