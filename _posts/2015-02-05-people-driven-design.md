---
layout: post
title: "People Driven Development"
categories: [UX]
---

As a developer, I hear terms like behavior driven development and test driven
development being used constantly. And these are good methods for building
software. The concept being you establish an expected behavior of your software
and write testing scenarios to prove your software meets that behavior. 

<!-- more -->

At first your testing will fail because you haven't written any software yet. But
this is good, this is the point. Now that you have failing tests, you begin
writing your software. At first, you do whatever it takes to get your tests to
pass, then you continue refactoring, or reworking, the software for easy
readability, code efficiency and reducing code duplication. This methodolgy
results in strong software that is easily maintainable over time. You can add
features easily and change your code because you have tests to prove that you
didn't break anything as you added those features.

And this is great, but it doesn't solve all the problems for our software. We
still have to address the user experience.

## User Experience

User experience encompasses the whole of what our end use goes through. From the
sign up phase to the phone call with customer support when the user struggles
with the software. But if we've done our jobs well in building software, the user should never have
to pick up the phone to call for support. And this is where we often fail in
developing software.

We spend time proving our ideas, doing research, perfecting the design, but all
this is worth nothing if we hold onto our bias when building the user
experience. 

## Bias

Even the most open minded person brings a bias to their work. We may try to
bring under-represented personas into our software building, but if we haven't
met or shared a particular point of view, it often gets left behind. In the
worst case scenario, we simply don't think about a user who isn't like the group
of people we fall into.

Bias is something we grow up with whether we want to or not. Our parents raise
us in a culture and with beliefs that they have. We go to schools built on a set
of beliefs defined by our governments, some of which are built on the beliefs of
people, but often times on the beliefs of those with power and money.

## Normality

The society we have built around us defines a view of 'normal'. Currently, in
the western world, normal is defined as heterosexual, white, thin, and spending
money you may or may not have. Most men want a 32 inch waist and a six pack set
of abs. Magazines tell women they should be no larger than a size 4, but have a
disproportionately large bosom. 

In the time following World War II, the ideal image was different. Women were
supposed to be like Marilyn Monroe&mdash;a buxom size 14 curvaceous
blonde&mdash;and the men were supposed to be debonair, dapper, fit but not
neccesarily with a six pack.

Turning back to the 17th century and the time of the artist Ruben, women like
Marilyn were considered to be frail and unhealthy. Men displayed their wealth by
the size of their girth. 

The reality is, people are extremely diverse. We come in all shapes, sizes,
colors, abilities and predilictions. Some of us are skinny, others not. Some are
tall, others short. Some are able to run a mile in 4 minutes, some can't walk on
their own. All of these people are normal and perfect because they are who they
are and unique and wonderful for all their differences.

## People Driven Development

I'm not the first to use this phrase. But my definition may not be the one that
has been used before. I'm just beginning to develop what this term means, and I
expect to evolve over time. 

People Driven Development has two main ingredients right now. 

1) It comes before TDD or BDD in development.
2) It must encompass extreme cases of users. 

Regardless if you think less than .01% of your user base may be affected, we
need to account for it. We develop our PDD plan before we begin defining the
user experience. The initial UX comes before we begin writing any code. As we
develop our product, we involve people in the testing of it. When we find a
scenario we didn't account for during development, we go back to PDD and put
that scenarion in. We revisit the UX definitions and improve them.

### So what is the point? 

If we make our product easy to use for the extreme scenarios, it will be eaiser
to use for the top of the bell curve, or the large portion of our customers.
Easier to use means less support calls, less leaving in frustration, and happier
experiences. These result in higher conversions and engagement, which leads to
higher revenue. You also reduce costs in support, refactoring you code,
complying with lawsuits and more. If the job is really done well and leaves
users feeling better than when they started, they will spread word of how great
the product is to their friends, collegues and neighbors again growing the user
base.

### So how do we do it?

I don't have the clear answer yet. I'm hoping to start a dialog and define it.

