---
layout: post
title: "Open Letter to Stewart Butterfield"
date: 2016-01-18 13:00
comments: false
categories: [Accessibility]
---

Yesterday, [Stewart Butterfield of Slack](https://medium.com/@ericajoy/a-message-from-stewart-butterfield-to-slack-employees-9f794f0a0821#.xpu9rnx7o) emailed his team letting them know that the San Francisco office would be closed in obsevance of Martin Luther King, Jr. Day. I think this is brilliant. He makes a good case for why more companies should be closed and we need to continue the fight for civil rights.

<!-- more -->

>These are people who have been beaten, and burned, and raped, and shot, and hanged because they stood up for their own basic dignity. Not people asking to take something from someone else. Not people threatening harm. People asking for an equal right to vote, to have freedom from violence, access to education and housing, and the right to make a living.

However, Stewart is talking about something that he himself can do better on. Race isn't the only place where people are struggling with civil rights. Disabled communities continue to be ignored and discriminated against, especially [disabled black people](https://www.jrf.org.uk/sites/default/files/jrf/migrated/files/1859353916.pdf). Disabled people are often denied their own sense of dignity and autonomy, that job discrimination often leads to this. Disabled people in particular do not get the visibility of those dealing with racism and the oppression by a militarized police that the Black Lives Matter movement garners, yet face many of the same issues including access to education, housing, the right to make a living, and the right to vote. The American Disabilities Act exists to prevent discrimination, but this doesn't apply to the web and technology. In fact the [Department of Justice has decided to table the decision](http://minnesotaemployer.com/2015/12/01/doj-delays-website-accessibility-regulations-under-title-iii-of-the-ada-until-2018/) on updating the ADA to include technology and the internet until 2018. Many, including Slack, are inadvertantly discriminating against people with disabilities because they are not making the web and technology able to be used by all people.

And this is what I want to point out. Slack has had a listing for a job of [Accessibility Product Manager](https://slack.com/jobs/78917/accessibility-product-manager) open for a couple of months now. I know many folks who have applied. I applied. We want to help make Slack better because we love it, but we are getting rejected. I'd love to help take a fantastic tool like Slack to the next level and make it something that facilitates freedom and communication amongst disabled people. But Slack is rejecting folks with years of experience simply because they want that person located in San Francisco. Because of this, the role has not been filled yet and Slack as a tool continues to have accessibility problems. As a tool geared towards company communication they are damaging themselves by not being accessible. Companies likely to use Slack are required to make reasonable accomodations that could include choosing another tool, thus limiting their own growth.

Stewart, as a leader in the industry and a builder of tools for communication, I implore on you to open your position of Accessibility Product Manager to remote people who have the experience to lead your company. You have the ability to take charge and lead the industry in proving that quality products can have quality user experiences that include accessibility, but only if you actually bring some help in you get there. 

### Note & Update

The issue at hand is often compounded for people of color with disabilities.
Disabled people are not excluded from having other discrimination against
them. Many of the people in the BLM are pushing forward rights for disabled folks. This is simply
about Slack's response to not filling a role it could have many times over
and discriminating against some people including black people that are disabled. We need more intersectionality. We need
to make sure we don't discriminate against anyone. Slack can do better.
