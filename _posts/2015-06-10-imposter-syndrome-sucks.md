---
layout: post
date: 2015-06-10 09:00
categories: confidence 
title: "Imposter Syndrome Sucks"
section: blog
---

But we do it to ourselves. I battle imposter syndrome every day. In fact I made a
distinct decision back in 2009 to **"fake it until I make it"**. I got fired for
poor performance (I was severly depressed at the time which affected my work). 
While out of work in a down economy, I specifically put on a new outward face that would pretend
to be someone I'm not to land a job.

<!-- more -->

> The reason we struggle with insecurity is because we compare our behind the
> scenes with everyone else's highlight reel 
> <cite>- Steven Furtick</cite>

I didn't lie about my skills or abilities. I was actually quite humble about
them. I didn't lie about my background or education. It just wasn't in my
nature. What I did was pretend I was outgoing. I pretended I had something to
talk about. I pretended I was interested in other people. 

And it worked. I got a new job. I established myself as someone who cared about
my team, and thus became a leader. I got on stage and presented my ideas to
people. I taught others what I knew. Over the last six years I have turned that
decision to fake it into quite a successful career. I've been invited to talk at
conferences throughout the United States and even in Scotland. I've gotten to
meet and work with some of my industry heroes. I get to hang out in Slack
channels and tweet with people whose knowledge I built my career on (and they
know who I am!). Something else happened, something I didn't expect, I actually enjoy these things, have the skills, and I am
interested in people.

But everyday, every time I try to do something, I feel it creeping in the back of
my brain, "You don't belong here. You don't deserve it. You didn't write a book.
You don't blog enough. They're humoring you." Its the voice of my lack of
confidence. I was shy and bullied a lot when I was growing up. It is embedded in
my psyche that I am insignificant. 

I have a rational brain though. I can look at my history and see the success I
achieved at other jobs, popularity in my blog posts, and the fact I keep getting
invited to join groups of experts or to talk. I am skilled. I am successful. Yet
the imposter syndrome still nags. I know I am not alone in this.

We need to stop comparing ourselves to others. Look inside and figure out what
makes you happy and do it. I really like speaking and meeting others, so I'm
trying to do more of it ([need a speaker? Email me.](/connect/)). I like
writing, so I'm working on a bunch of tutorials that could become a book with
the right editor. I'm asking you to do the same, no, I'm begging you to find
what you love.

Success is not what society tells us. You don't need to own a home (I want to
sell mine), or a fancy car. You don't need millions in the bank. You just need
to do what makes you happy. 

I like to make art (but I'm way out of practice), I'd like to learn the guitar
again, I want to travel the world, and I want to teach others (not just tech
stuff either). So over the next two years I'm going to be focusing on setting up
my life to do these things. Why two years you ask? Well, I have two kids in high
school, I want to let them graduate before I sell the house and start traveling.
Will you decide what makes you happy and try to set yourself up for that life?

