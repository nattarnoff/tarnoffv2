---
layout: post
title: "CSS Process and Workflow"
date: 2013-06-15 12:45
comments: false
categories: [Presentations]
---

For the last month I have been trying to spend more time with improving my workflow in front end development. Then at work I got the opportunity to present my findings to my team. I put this little deck together on [http://slid.es](http://slid.es), which uses reveal.js to make pretty presentations. 

I spoke about two tools (Sass and LiveReload) and one process (SMACSS) I have begun using on a daily basis.

<!-- more -->

<iframe src="http://slid.es/gregtarnoff/css-processes-and-workflows/embed" width="576" height="420" scrolling="no" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

##[Sass](http://sass-lang.com/)

I love Sass. The ability to create mixins, extend classes, use variables and compile the code to a compressed version is brilliant. Additionally using nesting to deal with hover & focus states and media queries has really improved my development speed. It sill takes time to figure out what goes where when and how things break, but at least now I can go to an element and know everything is right there. And with Sass, if it doesn't compile it tells me where to look across all my partials. It is definitely an organizational improvement.

##[LiveReload](http://livereload.com/)

I simply won't work without this or a comparable tool ever again. I just need to find a way to automate display of hover/focus/active states and I will never really need to leave my text editor again.

##[SMACSS](http://smacss.com/)

I'm still fumbling my way through this one. If you are using SMACSS, you need to use Sass as it will help organize things. That being said, naming things here is easier than I thought, but organizing what goes where I still find challenging. 
