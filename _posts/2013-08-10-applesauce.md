---
layout: post
title: "Applesauce"
date: 2013-08-10 22:23
comments: false
categories: [Cooking]
---
Nothing really goes better than [Mac & Cheese](/blog/2013/08/10/mac-and-cheese) than applesauce. One of my kids won't eat the store bought stuff, but loves my homemade version so much they made a bet with me with applesauce everyweek as their prize. They won.

<!-- more -->

##Ingredients

- 3lbs apples. I really like MacIntosh, but Gala or Fuji work in a pinch
- 1/4 cup Earth Balance
- 1/2 cup brown sugar
- Cinnamon to taste

Peel and seed the apples. Slice them into 1/8 inch thick pieces; thinner is ok, but don't go thicker. Melt the Earth Balance in the pot and add in the brown sugar, apples and cinnamon. Cook over medium-high heat for 45 minutes to an hour. Serve warm on its own, with crepes, ice cream, granola or any dish you want!
