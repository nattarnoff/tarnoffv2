---
layout: post
date: 2015-07-27 10:00
section: blog
title: "Animated Gifs in Social Media Streams"
categories: ux
---

Last week I encountered a pseudo 3D animated gif from [Doritos](http://twitter.com/doritos) that autoplayed while viewing my [Twitter](http://twitter.com/twitter) feed. This gif was brightly colored, and its “3D” effect triggered a vestibular attack and migraine. As I looked more into the issue, it isn’t just Twitter who does this. It also happens in our [Instagram](http://instagram.com), [Vine](http://vine.co), [Facebook](http://facebook.com), and [Snapchat](http://snapchat.com) feeds, just to name a few. Now some of you will tell me, “those channels are meant to work that way.” True, but what if due to advertising, or worse hacking, someone uploaded a gif worse than this Doritos one that autoplayed and triggered a photosensitive epileptic seizure? Who is responsible?

<!-- more -->

## Trigger Warning - Animated gif below

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Get ready for the new Doritos Jacked 3D Bacon Cheddar Ranch. In stores for a limited time only <a href="http://t.co/JqRkd5zj1W">http://t.co/JqRkd5zj1W</a> <a href="http://t.co/zMQgqWmgLP">pic.twitter.com/zMQgqWmgLP</a></p>&mdash; Doritos (@Doritos) <a href="https://twitter.com/Doritos/status/623180401507328000">July 20, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Go ahead, hit play, but don't say I didn't warn you. Now imagine if that was on
Justin Bieber, Kim Kardashian, or Taylor Swift's feed?

I would say whoever uploaded the gif is certainly responsible, but isn’t the platform as well? Do these really have to autoplay? When it comes to ads, is anyone checking to see if the ads meet certain standards? In radio and television, they don’t turn down ads because they don’t like the content, but if the ad promotes violence, cruelty, causes medical emergencies, or harm to the public, they will refuse to air it. I argue that those similar rules need to apply to the social networks we enjoy as a primary source of content.

[I personally want an apology from Doritos for causing me pain](https://twitter.com/gregtarnoff/status/623585809250168832). I know it wasn’t done on purpose, but that doesn’t mean it couldn’t have been avoided. Good marketers and user experience professionals need to know the audience and if their audience might have a medical condition that this type of ad would bother, then need to change that ad. 

I’m also asking Twitter, Vine, Facebook, Instagram and all other social media platforms to put controls that prevent autoplaying of videos and gifs. The user needs to have control over their experience if movement may trigger physical pain. 

And for those of you saying I shouldn’t use the Twitter web interface, why is it there if not to be used? Typically I use my phone, but as a consultant I am on many different machines in a week, some of which can’t have clients installed, and sometimes I’m in buildings where my phone won’t work for security reasons. 
