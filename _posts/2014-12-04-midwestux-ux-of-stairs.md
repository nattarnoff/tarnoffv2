---
layout: post
title: "MidwestUX - UX of Stairs" 
date: 2014-12-04 10:56
comments: false
categories: [Presentations]
---

I took a little time off from speaking due to the new job and how difficult it
was to guage my abilities given my diagnosis of Chronic Subjective Dizziness
(more commonly known as forever having vertigo without a reason). But this
summer as I became more aware of my abilities and the story I have to tell, I
started up again. 

<!-- more -->

I developed a talk that tells my story and demonstrates how I feel on any given
day. I walk the audience through what it means to be disabled and how the world
around us, physical and digital, is set up against people with disabilities. And
hopefully, by the end I have inspired them to do something about it.

I was lucky enough to have this talk accepted at That Conference and MidwestUX.
Below you can see the recording from MidwestUX.

---

<iframe src="//player.vimeo.com/video/110360523?title=0&amp;byline=0&amp;portrait=0&amp;color=ff9933" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

---

If you want to ask me questions, hit me up on Twitter
[@gregtarnoff](https://twitter.com/gregtarnoff).
