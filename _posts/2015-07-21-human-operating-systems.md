---
layout: post
date: 2015-07-21 10:00
section: blog
title: "Human Operating Systems"
categories: empathy
---

When I speak with people about my chronic illness, there is often confusion on it. Many people think, “your pain [or dizziness] is just in your head” and that I can just let it go or ignore it. Yes, my disabilities are invisible, but that doesn’t mean they aren’t very real. When we meet people who are deaf, we can’t see what is physically wrong with them, but we believe them when they say they can’t hear us. Why don’t we believe people with other disabilities?

<!-- more -->

While at a meeting recently with a group of parents, we conducted a mindfulness exercise. We meditated on an itch we had on our body. The goal was to sit with the stress an itch can produce and yet not let it bother us. We couldn’t scratch it either. For some parents, the itch went away. Others let it bother them. I enjoyed the sensation, but then I think I have an unfair disadvantage that the group leader knows about and agreed.

One of the parents was curious, and since I’m not exactly shy about speaking of my disabilities, I expanded on it being chronic pain and dizziness. This father blew off my feelings and experience right away saying “It’s all in you head” meaning that I was making it up. I countered that it is in my head, but very real and that if I could wish it away I would in fact to do that. 

##Human Computers

This began a discussion that had me communicating in a way that seemed to connect with the other parents. If you have seen my talk, UX of Stairs, you have heard me mention the idea of human hardware and software. We are all really complex computers. We are made up of two main systems that are fairly standard.

The first is our hardware. We are all initially connected with the components needed to grow our biological hardware the same, but due to environmental changes, sometimes that hardware doesn’t develop the way it should. After birth we encounter a gigantic world that too is going to influence our hardware. But for the most part we are born with 5 input systems (5 senses), and two output systems (oral and physical).

We also come with a base operating system. One that turns the hardware on and keeps it running. It can take input and translate it to output. But most importantly it can learn. It can combine the input with algorithms we call emotions and creates a new output that is a subclass that we call art. And art comes in many forms. For some it is a visual output like drawing and painting, others produce auditory output like music or language, and still there is the expansion of the core physical output in the form of dance or sports. With training, anyone can process any input through emotions and produce any of these types of output, but for some it comes more naturally than others.

Once in a while you encounter something in your life that affects your software just as it could affect your hardware. This could be a happy event, or perhaps a traumatic one. This experience alters your input, output, or algorithms in such a way that when you experience it again (or something that reminds you of it), your reaction won’t be the same as someone sitting next to you who hasn’t shared that experience. 

For some people, this event can simply be suffering lots of pain. Over time, you develop a tolerance for that pain. Sometimes, you are stubborn, like me, and try to move through it when you can’t make it go away. Additionally, your hardware, nerves in this case, may be more or less sensitive causing the input, not the algorithm, to give you a different tolerance.

My dizziness is a matter of my software being rewritten. The vestibular system is made up of our inner ear system, ocular system, and our sense of spatial awareness. Together these form a kind of internal gyroscope. Thousands of points of data are coming in through these systems every second of every day. Your brain needs to clean that data up and process it. 

You subconsciously apply a filter to the incoming data and remove the outliers and extremes. This reduces the data to an average that can be easier to work with. I think this is how my algorithm actually works, except that my filter needs new parameters of what to filter. Right now, it doesn’t filter much. A couple years ago, somehow my settings loaded a new configuration file that turned the filter off completely. Over the last 30 months, I’ve been slowly adding them back in through experience and direct training. This makes sense to me as I have been able to slowly tolerate more experiences.

So what happens when my filter is broken? Imagine you are making coffee in the morning. You have your filter in the pot with the coffee in. As you start to pour the water in it strains through the coffee first, then the paper. But the coffee and the paper can only take so much water at a time. If we pour too fast we begin to see the water backup, the coffee grounds float, and eventually the water/coffee combo flow over the sides of the pot. This is what is going in my head. With too much stimuli or when a filter isn’t working properly, I simply can not digest the data coming in. Something will get lost and a mess follows.

Most of the time this means I experience aphasia. Aphasia comes in two forms, receptive and expressive. Receptive aphasia means I literally won’t know what you are saying or doing (usually words). I won’t recognize the sounds, or that they are in fact words. Any I do catch will not register their meaning. “In one ear, out the other.” is probably the best description of this.

Expressive aphasia is when I’m the one doing the communicating. Typically I stop in the middle of a thought or sentence. I’ll wave my hands wildly and move my mouth, but nothing other than gibberish comes out. I can see the concept right in front of me, but I lack the proper words to express it. 

While technically the software is in my head, anyone who has tried to program anything (even a VCR clock), knows that it isn’t always as simple as forgetting the current settings and putting in new ones. With learning systems, we can’t delete those files, otherwise we corrupt the whole system. We must slowly reteach things to overwrite the current configuration. The algorithm will never be the same as the original as it always retains the history of what it previously learned, but with enough training, we can get close to the original. This is my hope.

Humans aren’t copies of each other. We are each unique in our hardware and software. The next time you meet someone whose software isn’t running the same as yours, instead of blowing them off, take a moment and try to understand how and why they process things differently. It will help you empathize and understand them. It will also help you expand your algorithms to make you a better person and technologist.
