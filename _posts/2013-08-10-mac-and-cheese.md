---
layout: post
title: "Gluten Free Vegan Mac &amp; Cheese"
date: 2013-08-10 22:06
comments: false
categories: [Cooking]
---

One of my favorite dishes has always been Mac & Cheese. Growing up we'd have the classic Kraft box dinner on a regular basis, but I only ate homemade mac & cheese if we went to a family get together. One of my aunts made it with the crusty breadcrumb top, but hell if I can remember which one it was. 

When I began cooking for the kids, I actually looked at what was in the Kraft dinner and decided I could do better. When I went vegan I longed for the dish and eventually found a reasonable cheese replacement and enjoy it we did a couple times a month. When Sara joined us, I needed to find a gluten free version. We now eat this once a week.

<!-- more -->

##Ingredients

- 12 - 14 oz Gluten free shells or elbow pasta
- 1/2 cup rice milk
- 1/4 cup Earth Balance
- 1 cup frozen peas
- 1 package Tofu Pups
- 14 oz non-dairy cheese (We use the [Go Veggie! shreds](http://www.goveggiefoods.com/))

Preheat the oven to 350&deg;. Cook the pasta according to the directions. We use rice pasta and it cooks very different than a semolina wheat pasta, so follow the directions on the bag. While this is cooking, cook the Tofu Pups by boiling them in water. Remove the Tofu Pups and slice into bite size chuncks. Once the pasta cooked, drain and rinse it. While it is draining and the pot is still hot add the rice milk, Earth Balance, peas and Tofu Pups to the pot and put it back on low heat. Add in about 2/3 of your cheese to start it melting. Once it gets a little gooey, add the pasta back in and mix it all up. Take the remaining cheese and sprinkle it across the top. Bake in the oven for 10 minutes, serve and enjoy! I like mine with my [homemade applesauce](/blog/2013/08/10/applesauce).
