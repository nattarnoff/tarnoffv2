---
layout: post
title: "Learning to Let Go"
date: 2013-08-17 17:06
comments: false
categories: [Health]
hero: "https://farm3.staticflickr.com/2202/2141537474_bdb5243d81_o.jpg"
---
In the Beginning...
For ten years I have been dealing with chronic pain in the form of daily, debilitating migraines. Unlike some people who get migraines every couple of months that may last a few hours to a few days, my migraines show up every. They come in swaths of anywhere from as few as three and up to as many as eighteen in a single day and they would last from 15 minutes to three hours.

<!-- more -->

The pain is so great, that my nerves have forgotten how to turn themselves off in between headaches. I am left with a dull -- sometimes numbing -- headache between these migraine spikes. It became so prevalent, I stopped referring to the in between time as an actual headache despite it meeting the very definition.

##March in the Clowns

Over the last decade, like many people, my employment and my benefits have fluctuated. When I have insurance, I consult with doctors. The first few years it was just a primary care physician, but eventually he gave up and referred me to the neurology department at the medical college in Milwaukee. The doctors there were pleasant enough people, but treated me like any other migraine patient despite showing signs of it not being a normal case. 

They tried a gamut of anti-migraine medicines. Some were suppressive to take daily, some were abortive to take when I had a headache coming on. The suppressive ones would work for about two weeks. I could feel the dull constant pain coming down to a level approaching zero. However, by this time I was so sensitive  in those nerves, the curve of descent was actually a parabola that never hits zero, it just gets unbelievably close. 

Then there were the abortives. Nine out of ten times, they did nothing. The few that did caused odd side effects. They may have worked on getting rid of the headache, but when you are hallucinating in a meeting with the CEO of your company you really need to ask if the medication is worth the effort. I decided it wasn't. Then there were the abortives the insurance companies didn't like me to take. They were upwards of $100 per pill with a monthly limit of 4 pills. That is $100 *after* the insurance company paid their part. And when you have 4 headaches a day…well the monthly allowance just didn't go that far. 

Then I moved to Madison. Since I had my old records, my primary care provider under this new insurance plan didn't bother treating me at all and just referred me to the University of Wisconsin Pain and Headache clinic. I was to be treated by the best minds in the state. 

The first diagnosis was Paroxsymal Hemicrania. This means "sudden headaches on one half the head". It is a subset of migraine apparently. And we began the regimen of trying solutions. Then the diagnosis changed to "Hemicrania Continua", or rather "continuous headaches on half the head" (Latin can make anything sound impressive). I was given NSAIDs, anti-depressants, epilepsy drugs, triptans, fibromyalgia drugs, muscle relaxers and I stopped tracking after that. Most of the time they did nothing. Enough of the time they caused side effects. 

One medication had a known side effect of drowsiness, so I was instructed to take it right before bed. If I remained drowsy the next morning, I was take it an hour earlier. After one dose, I was still a complete zombie over 20 hours later. They never let me take a second dose. Then there was the anti-depressant that seemed to work at 25mg. So they upped me to 50mg in hopes that by the time I reached the "normal" dose of 200mg I would have no pain. Two doses at 50mg and I was having thoughts of suicide, depression and harming others. It took three weeks to make those go away even though I didn't take any doses after that. I stopped breathing one night after taking the prescribed muscle relaxer, had it not been for my partner, well I don't like to dwell on it. Then there was the epilepsy drug that made my nerve endings all fire off so that the slightest air movement, heat, cold, clothing, even light would cause me pain. I developed the shakes and laid awake most of the night with a fever. I was taking 400mg a day of that one and could take up to 600mg per day. A "regular" dose is 900mg per day or more. 

My providers at the headache clinic are quite patient, but when I call, they don't want to answer the phone. I have been dubbed "less than 1%" because I am that guy who experiences the side effects listed in drug ads as "less than 1% of patients have experienced the following side effects". 

Regardless of the myriad of attempts at fixing my headaches, the pain I have suffered, while despite putting me into the "severe disability" category, ever stopped me for more than a day from doing my job as a programmer or father. 

##Riding the Wave

Throughout my headaches, when they were at their worst, I would experience some dizziness and nausea. Beginning around Christmas of 2012, this dizziness became more and more frequent. By April, the sensation of standing atop a large boat undulating on gentle waves was almost constant. To add to this though, I would have moments of severe dizziness where the entire world would rotate in my vision counter clockwise as if I were looking through a kaleidoscope. I would reach out for a wall, table, chair or anything solid that could support my 220 pounds. My partner noticed the severity before I did. 

<img alt='My disabled parking tag' src='http://farm4.staticflickr.com/3751/9259968524_8c70298704_n.jpg' class="left"/>

Every day the dizziness would get worse until I was no longer having it just with headaches. Waves of this kaleidoscopic sensation would hit me as many as a half dozen times a day. By the middle of June my doctors referred me to ENT specialists. I began to fall during normal, routine activities. My legs would go weak during the worst spells and I'd lose my balance. I have been fortunate so far in that none of my falls have resulted in injury. One of these days my luck will run out. 


At the end of June 2013, it became clear things needed to change. I had tests of my vestibular system scheduled, but I had to stop driving. At the same time, I became so unstable on my feet that I started using a trekking pole as a cane to go as little as 5 feet. July came and I was put on medical leave from work for two weeks and issued a disabled parking tag.

###Letting Go

Ego is defined as a person's self esteem. We build this up by proving what we can do and how we present ourselves to the world. For some it is their ingenuity, others it is their athleticism. For most of us, we just want to fit in and be "normal". "Normality" gives us an appropriate level of ego and we clutch onto it like we would our children and families. "Normality" allows us to be confident without being cocky, without being an idiot. 

I thought I defined my ego by being a father, partner, friend, photographer, son, brother, and programmer. However, as I began to lose the ability to do things, I found that while those are parts of who I am, my ego holds onto much smaller things. 

Now, I stroll the halls of the office wobbling on my cane refusing to give up being under my own power of locomotion just yet. This means often crashing into filling cabinets or stopping to rest against walls. As I do this, people stare at me. If they know who I am they ask, "What's wrong with your leg?". "Nothing," I reply and keep hobbling. I'm sure they mean well, but I haven't reached a point of comfort to discuss my new level of inability. 

This of "can't dos" grows daily. I don't have the strength to stand up to cook or do dishes. I have just enough to brush my teeth if I hold onto the counter. My partner needs to either help me down the stairs, or fetch my things and bring them up. Watching television, scrolling the computer or my phone can cause me to have a severe vertigo spell. With each bad attack I begin to panic, which causes confusion. Too much stimulus, usually visual or audible (I hate crowds), and I begin to shut down and lose what little stability is left. Going to the grocery store is a challenge. Half an hour in a large store and I'm so exhausted I feel like I rode my bike 10 miles. 

But each day, I find something I can't do or can't do in the same way I used to. Each time I discover a new thing that is difficult, it is a blow to my ego. But I take a deep breath, change how I do it to be safer, and try to release this ability from defining part of who I am. Because underneath it all, I am still a father, partner, friend, photographer, son, brother, and programmer. I can still guide and love my children as they become who they're meant to be in this world. My love for my partner has only grown as she has supported me in more ways than I could ever ask, and in turn I give her everything I can no matter how little that is. I can still have a good time with my friends. I can still write and teach others to write good code to build great things. This is who I am, it doesn't matter if I can drive, walk, ride a bike, hike the mountains or roller-skate. I am disabled, but I am not defined by my disability.
