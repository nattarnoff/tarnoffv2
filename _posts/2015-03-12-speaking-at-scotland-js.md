---
layout: post
title: 'I&rsquo;ll be Speaking at Scotland JS'
date: 2015-03-12 09:15
comments: false
categories: [Public Speaking]
---

I'm super excited to officiall announce that I'll be presenting my &lsquo;UX of
Stairs&rsquo; talk this May at [Scotland JS](http://scotlandjs.com) in
Edinburgh. This will be my first time leaving the United States other than a few
trips to Ottawa, Canada when I was a kid, but does that really count? Just 3
days after [CodePaLousa](http://codepalousa.com) in Louisville, KY I'll be
boarding a flight across the pond.

<!-- more -->

!['Scottish castle ruins on a lake with fog rolling
in'](/img/10_scotland_photo.jpg)

This is a great opportunity, and I hope to see a bunch of you there. Scotland is
actually number two on my list of must see places (Nepal is first), so this will
be awesome to check it off my bucket list. 

Due to schedules, I won't have time to do a lot of sightseeing, so this trip
I'll miss out on the highlands and Islay regions. It is possible I could see
Glasgow one day, or Hadrian's wall if I meet some really enterprising locals. I
will have most of the day before and the day after the confernece to sightsee,
so I'd really love connecting with some locals to show me the good stuff (Scotch
of course).

If you'd like to connect before the conference, [tweet
me](https://twitter.com/gregtarnoff) or [send me a message](/connect/). I can't
wait for May!

Can't make it to Scotland? There is still time to
[register](http://codepalousa.com) for CodePaLousa in
Lousiville. Use the code __GTarnoff__ for 10% off.
