---
layout: post
title: "How to Write About Accessibility"
date: 2016-01-28 14:00
comments: false
categories: [Accessibility]
---

Over the last year or so, there have been a lot of people starting to take
accessibility seriously, which I love. Accessibility is showing up in blog
posts, Github repos, frameworks, and styleguides. But sometimes the way they
write about it minimizes the impact that a disability can have on an individual
trying to get by in the world.

<!-- more -->

Accessibility covers many things. Typically when talking about it on the web,
most developers think of visual difficulties. Primarily blindness, but also
color blindness, and hopefully low vision. But there is a long list of things
that could affect the user and how they interact with your product. This
includes, but is no means limited to:

- Vision abilities
- Hearing abilities
- Physical limitations
- Cognitive abilities
- Neuro-diversity
- Learning disabilities
- Mental Illness
- Access to technology
- Economic status
- Slow internet connection

> "Accessibility includes designing for people who...are drunk, or undercaffiented."

When we use phrases like, "Accessibility includes designing for people who ...
are drunk, or undercaffiented," we minimize the struggles people go through when
affected by disabilities. I realize that language like this is meant to be light
hearted, playful, and make accessibility more tanigible to the able bodied. 

However, this type of communication shows the privelege that writer has. It
makes it seem that someone who is blind since birth is in the same situation as
someone who hasn't had a latte. It equates over indulging in wine with having
Down Syndrome, even if it isn't intended that way.

In order for accessibility to get better, we need able-bodied developers to
have empathy and understand the entire audience for which their technology is
built. But instead of minimizing disabilities, why don't we use language that 
the developers can relate to that are real issues? 

## Situational Disability

A situational disability is something that isn't permanent. It can be a small as
a night of sleep deprivation, but more often it is an injury or limitation a
person has. For instance, how does your site work for a right handed person who
broke their wrist? What if you are a new mother who is trying to feed her baby
while writing emails? Suppose you were out and your glasses broke, how would you 
 end up using your phone differently?

These are real world situations that can be considered temporary disabilities.
They are things that the average peson can relate to, but they directly affect
how we think about technology. The person whose glasses broke is now a person 
with low vision. The broken wrist has limited keyboard access, mobility, and dexterity
issues. The mother needs to use voice command software because her hands are
full. These scenarios we can imagine ourselves in, but they mirror how those
affected by blindness, Parkinson's, Spinal Muscular Dystrophy, Epilepsy, and ALS, 
along with other disabilities, would relate to our products. 

Let's move towards using real world examples of situaional disabilities rather
than jokes when discussing accessibility. It treats everyone with more respect
and provides a dialog people can still understand.

Disagree with me? Talk to me about it on
[Twitter](https://twitter.com/gregtarnoff).
