---
layout: post
date: 2015-08-03 10:00
section: blog
title: "Infinite Canvas 6 With Rachel Nabors"
categories: screencast
---

Last week I got to be on a podcast. This week I appear on a screencast with
[Rachel Nabors](http://twitter.com/rachelnabors) talking about vestibular
disorders in general and how animation may affect a user on your site. 

If you don't know who Rachel is, she is an amazing illustrator, cartoonist,
speaker and animator using all those skills to shine a light on the web can be a better place with the right kinds and amounts of animation.

<!-- more --> 

We had a lot of fun recording it, but I need to get a better microphone, sorry
for the scratchiness that my beard brings. Also, we had a few problems trying to
  record it. Running Skype, Camtasia, Quicktime, and all the websites we looked
  at kept crashing our GPUs.

The screencast is on [Youtube](https://www.youtube.com/watch?v=QhnIZh0xwk0), and you should totally subscribe. She hasn't done one in a while and it could be another while before we see another, so unlike the podcast episode I did, I'm embedding the video here.

<iframe width="560" height="315" src="https://www.youtube.com/embed/QhnIZh0xwk0" frameborder="0" allowfullscreen></iframe>

Enjoy! Hit us up on Twitter with any questions you may think of. 
