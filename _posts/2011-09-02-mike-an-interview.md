---
layout: post
title: "Mike: an Interview"
date: 2011-09-02 06:07
comments: false
categories: [Accessibility]
---
I needed new tires on my car. Instead of waiting inside the stale waiting room of the auto repair shop with the greasy chairs and burnt coffee, I decided to head across the lot to the mall for some food, tea, and free wifi to get some work done. While eating my lunch, I noticed a gentleman with severe motor control issues using an eReader to enjoy a book. Being a developer focused on user experience and accessibility, I thought this was a prime opportunity to conduct a short user interview. Looking back, this was pretty selfish of me. The next 10 minutes were an awakening for me.

<!-- more -->

After I finished eating, I approached the gentleman and asked for a moment of his time. His name was Mike and he graciously allowed me to interrupt his reading. I introduced myself and explained what I do for a living. I hadn’t met anyone with his condition using an eReader, so I told him I wanted to learn about his experience.

He was using a Kindle DX, which is Amazon’s deluxe model. And his first thoughts? Mike loved it. He reads a lot and given his limited arm and almost no hand control, using a regulate paper book was very difficult for him. Mike also had limited head control, difficulty speaking, was confined to a wheelchair (he has recently received a motorized chair and “drives” himself everywhere) and had a service dog. In addition to the general love of the Kindle, he went on to mention a few of his favorite features: the large home & previous buttons, and the very large next button for navigation. Despite his limited control, he is able to hit these buttons easily enough to enjoy his books. He also enjoyed that the Kindle displayed a whole page and had a decent font size selection.

However, Mike did have a one complaint about the Kindle. The Tic-Tac sized keys on the QWERTY keyboard below the screen were completely unusable to him. He didn’t have the coordination to hit the keys successfully.

At this point I segued the conversation to web application design. For him, link size isn’t an issue because he navigates websites via a trackball. The trackball gives him control over the sensitivity and speed of the cursor. However, despite this added control, he would like links and buttons to be bigger.

The last thing I asked Mike was if he could have his way, what would he like to have different in technology. His answer wasn’t what I was really asking, but it was extremely eye opening. Mike wanted to sell his Kindle and purchase an iPad. He likes that it isn’t just an eReader. He likes that it offers up Amazon, Barners & Noble, Kobo, and the tons of free books he could access. He also likes the apps. He likes the keyboard in that when he is on a key, he can see it pop-up above his finger. He likes that if he hits the wrong key, he can slide his finger until he is at the right one and only when he releases does the iPad acknowledge it being typed.

In addition, and more importantly for Mike, was the way you interact with the device. The swiping gesture used in many applications (specifically Kindle and iBooks) was much easier than hitting the Kindle’s large buttons. However, the shorter battery life and the display shutting off were things Mike identified as possible problems for him.

The experience of interviewing Mike was incredible for me. As a developer it showed me ways to think that I hadn’t considered before. It will make me a better programmer, and more importantly a better person. However, for me the best part of this 10 minutes & my entire day was the reaction from Mike. The fact that I approached him, respected him, and was genuinely interested in him was evident on his face. By truly caring about his experience in the world and taking a few minutes to talk to Mike, I brightened his day. Most people don’t give him any time. Most people avoid him like they might catch something from him. These are people too, we owe them our time, our respect, and our love. Show a little compassion and they will make your life richer.

My last note on this experience was mike’s t-shirt. He had on it the classic evolution illustration with the final figure being a person in a wheelchair. Inside the wheel was the symbol for equality. Underneath was the quote “Adapt or perish”.  As society I think it is imperative we take both the concepts of equality and “adapt or perish” to heart. We need to keep moving forward making life better for all or we will all suffer the consequences.
