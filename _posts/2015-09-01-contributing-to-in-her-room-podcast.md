---
layout: post
date: 2015-09-01 10:00
section: blog
title: "Contributing to In Her Room Podcast"
---

My friend, [Sara Blackthorne](http://sarablackthorne.com), has been running a
podcast for over 6 months now. She has 25 weekly episodes under her belt at [In Her
Room](http://www.in-her-room.com/) where she holds "meaningful conversation with women writers from around the world" (she is a much better writer than me, so I'll use her words) and it is all community supported.

<!-- more -->

Sara is doing this because she has to. It is a passion,
something she feels strongly about. She isn't doing it for money. She does it
because she loves it.

All things we create come with a cost though. I watched her pour her money
into recording equipment, editing software, web hosting, file hosting, and advertising to get
these amazing conversations with renowned writers out into the world. 

She puts research into each episode, making sure she is familiar with the
guests' work, history, and story to bring forward questions that help you, the listener,
understand what it means to be a writer, maker, creator, and woman in this
world. 

Like many podcasters, Sara uses Skype to conduct her conversations that in turn become
the episode. A few potential guests have requested to record by phone, meaning
to get the quality recording she needs Sara has to buy Skype credits. As a freelance
writer and editor, these little costs add up and mean sacrifices elsewhere. 

<img src="http://static1.squarespace.com/static/55734b32e4b082d12f5d477e/t/558b634ce4b087e32d5eb90a/1435198285884/in-her-room-patreon.jpg?format=500w" class="right" />

## Sara does this ad free

She wants it to remain ad free, because it is for you
the listener. But she needs your help. I've listened to the show and I'm blown
away by it. It is super inspiring. I see the excitment Sara gets bringing each
week's episode to life. This is why I am supporting her on 
[Patreon](https://www.patreon.com/inherroom?ty=c). I want this show to live and
thrive, but she can't do it without our help. 

There are about 500 people who listen to the podcast on a regular basis. She has
weekly shows, 4 - 5 per month, and if each of those listeners could pitch in $1
per episode, the cost of a cup of coffee, Sara would have plenty to cover the
costs associated with the podcast. 

There is another way to support her if you aren't comfortable with Patreon, hire
her. I rely on her to edit and gut check some of my blog posts here, but I also
work with her on my talks. She is really a kickass editor that will bring your
writing to higher level, not just checking to see if you have the correct
grammar. 

So, right now, please do one of two things: [go hire Sara](http://sarablackthorne.com/), or [contribute to In Her Room on Patreon](https://www.patreon.com/inherroom?ty=c) so that the podcast can keep going ad free.
