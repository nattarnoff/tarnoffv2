---
layout: post
title: "Remembering Those That Came Before Us"
date: 2012-10-23 13:43
comments: false
categories: [Remembering]
---
Yesterday one of my closest friends lost someone in her family that was a second mother to her. It was also the 12th anniversary of my father’s mother passing.

For me, this isn’t a sad day. I’ve dealt with it and have been able to move on. But I wanted to share how I coped with my friend as it is very fresh in her mind.

<!-- more -->

Twelve years ago my father’s mother passed. During the services many of the family shared their favorite stories and idiosyncrasies of my grandmother. There was a lot of crying and missing her, but there was even more cheering, singing, and laughing because she was such an incredible woman. This caused a rift in my family as some felt we didn’t take it seriously enough. But what I learned from that moment was that it is important to find the good in a person’s life and grasp onto it. Celebrate the life instead of mourning the loss. Now when I think about her and the others I have lost, I think about the moments we shared that were full of fun and joy and were premier examples of who that person was.

At first this brings a tear at missing the individual, but then my heart wells up with emotion for having been able to share those great memories and a smile emerges. I’m not sad. Our life is brief on this plane. If we spend it clinging to the sad, we won’t be able to enjoy it. If we enjoy the happy, it will have made this time worth having.
