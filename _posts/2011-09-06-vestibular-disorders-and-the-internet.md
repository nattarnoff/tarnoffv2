---
layout: post
title: "Vestibular Disorders and the Internet"
date: 2011-09-06 12:03
comments: false
categories: [Accessibility]
---
5by5.tv hosts Jeffrey Zeldman’s “The Big Web Show”.  [Episode #55](http://5by5.tv/bigwebshow/55) from September 1st had on budding web designer Marissa Christina of [abledis.com](http://abledis.com/) to talk about living with and trying to become a web designer with a vestibular disorder. The program went back on forth on what it is like, becoming a designer and what we as a community can do to make this hidden disability easier to live with while on the internet. I have extracted a few of the important things on building a better web for this group.

<!-- more -->

First off, you are probably wondering what a vestibular disorder is (I know I was). An excellent resource is the [Vestibular Disorder Association](http://vestibular.org/) and they have this to say:

>The vestibular system includes the parts of the inner ear and brain that process sensory information involved with controlling balance and eye movements. If disease or injury damages these processing areas, vestibular disorders can result.  Vestibular disorders can also result from or be worsened by genetic or environmental conditions, or occur for unknown reasons.

Vestibular disorders are issues with balance. According to Marissa, it is like being drunk all the time in that you are dizzy, have a loss of balance, and headaches. These family of diseases affect 1% of the population which is roughly 3 million people here in the United States. Currently there are no tools to simulate what it is like to have a vestibular disorder for web designers & developers.

Marissa says that when designing sites to avoid the following:

- Blinking and flashing elements (similar to designing for those with epilepsy)
- No loud noises
- No Flash animations or moving elements
- Avoid backgrounds that can cause optical illusions like checkerboards, swirling patterns or Las Vegas carpet style graphics

**Update**

Marissa was gracious enough to comment below and included some more accurate numbers from VEDA:

>“Approximately four percent (almost eight million) of American adults report a chronic problem (lasting three months or longer) with balance, while an additional 1.1 percent (2.4 million) of American adults report a chronic problem with dizziness alone.”
