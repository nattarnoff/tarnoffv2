---
layout: post
date: 2015-08-04 10:00
section: blog
title: "Presenting Designing With Empathy at OSFeels"
category: speaking
---

I've been working on a new talk called "Designing with Empathy" that covers a
little more than accessibility, but addresses the needs of those using assistive
technology as well as those who have difficulty with technology. We all carry
biases with us and when we build new sites, tools, apps, or games those biases
leak through no matter how hard we try and prevent it. I hope to introduce some
thoughts and ideas on how to reduce the influence of that bias as much as
possible in your next project and I'm super excited that the fine folks over at
[OSFeels](http://osfeels.com/) have asked me to present this talk to their
audience.

<!-- more -->

OSFeels is a two day conference in Seattle this October (2nd and 3rd) and
tickets are only $55.00 per person (This is a steal for a two day conference
with this caliber of speakers).

Many of us use open source software every day. I know I do (This site is built
on Ruby, Jekyll, HTML, CSS, JavaScript, jQuery, just to name a few). Open source
software held a promise for the future of beautiful applications maintained by
the community that used them. Unfortunately some of those communities have
become less than the utopias we though they could be. Working on these projects,
and software in general, can be a big toll emotionally. With rampant ablism,
sexism, racism, homophobia, and transphobia throughout our industry people can
expend their energy just proving they deserve to exist in the industry and our
software is affected by this. OSFeels hopes to open eyes and address some of
these issues so that we can all be better people building better communities and
software for everyone to share.

