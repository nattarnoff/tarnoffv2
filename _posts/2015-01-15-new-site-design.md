---
layout: post
title: "New Site Design" 
hero: "/img/pool.jpg"
comments: false
date: 2015-01-15 10:52
categires: [Design]
---

A little while ago I introduced a new design to my site using a bunch of tricks
I had learned over the last year. It didn't feel like me, nor did it feel
complete. So I did it again. Here are a couple of screenshots if you missed the
last revision, hopefully it is cleaner now. It certainly feels more like me.
It's not perfect, but neither am I. It's not done, but neither am I. I'll keep
iterating on it, just like I do with everything.

<!-- more -->

<img src="/img/v1-1.png" alt="" />

---

<img src="/img/v1-2.png" alt="" />

---

<img src="/img/v1-3.png" alt="" />

---

<img src="/img/v1-4.png" alt="" />

