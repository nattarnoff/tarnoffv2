---
layout: post
title: "Happy New Year's 2015 Edition"
hero: "/img/lights.jpg"
comments: false
date: 2015-01-01 00:00
categories: [Art]
---

Two thousand and fifteen. 2015. It will be a big year, I hope. I turn 40 this year. My baby boy turns 18.
My little girl turns 16. All important birthdays. I'm also dedicating this year
to tapping into my creative and design side more. For 20 years I barely picked
up a pencil to draw due to how I was critiqued by a single person. I've let that
drag on me for far too long.

<!-- more -->

I think I'm going to start with hand-lettering and pepper in other things as I
fell like it. I've already started and my family is super behind it by buying me
a huge Sharpi marker collection and new sketch book. I've been doing a lot of
calligraphy to get a feel for letter forms. I've been looking at lettering on
Pinterest and Behance. I'll be posting my own work on
[Behance](https://www.behance.net/gregtarnoff) as I get it done. Here is a quick
one I did in my new sketchbook.

<img src="/img/happy-2015.jpg" alt="Happy New Year" />

I hope you year is as good as I'm going to make mine.
