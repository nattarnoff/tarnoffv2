---
layout: post
title: "New Stickers"
categories: [Design]
---

A while ago I made some stickers of my social media avatar so that I could pass
them out as business cards at conferences. They were so popular, that I even
sold a few via my site. Well today I added another sticker to my collection.

<!-- more -->

<img src='/img/wi-beards.jpg' class="left" />
<img src="/img/logo12.png" class="left" />

-----

So now I have two stickers and I've moved the shop over to [Big
Cartel](https://tarnoff.bigcartel.com) to handle all the processing goodness.
I'm hoping to add more as time allows. For now you can pick up my classic avatar
or my new *Wisconsin Beards Best* design.

-----

<a class="button" href="https://tarnoff.bigcartel.com">{% icon fa-shopping-cart %} Buy Stickers</a>
