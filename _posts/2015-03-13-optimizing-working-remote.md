---
layout: post 
title: 'Optimizing Working Remote'
date: 2015-03-13 11:15
comments: false
categories: [Working Remote]
---

For the last eighteen months, I have been working remotely from my home in
Wisconsin for a startup located in San Francisco. This has been one of the
greatest adventures I have had in my life; learning a tremendous amount about the
world of startups, better coding practices, how to make a remote environment
work, and about myself. Today I want to share what I feel are best practices for
myself & the teams I'm on working with to get the most out of being remote.

<!-- more -->

We live in the information age. The majority of our work is dealing with
information and finances. The average worker goes into an office and works on a
computer located in that office. However, there is nothing about that office
that requires the person be co-located with all the other employees. This is
especially relevant in the world of creatives.

Designers, programmers, UXers, and writers are in a particular advantage of not
needing to be co-located. The tools that have sprung up in the last few years
allows them to collaborate real time from thousands of miles away. Meetings can
be held and accomplished with cheap (if not free) video and audio conferencing,
unlike a decade ago when it cost tens of thousands of dollars for a quality,
yet unreliable rig from Cisco.

Recent studies actually show the trend in &ldquo;Open Office&rdquo; plans are
actually harmful to productivity, primarily in creatives. Most of them need to
get in a zone to produce their best work. The lack of office doors, intrusion of
email and IM, and chaos of noise consistently pokes holes in a person's ability to
achieve this zone. Working remotely can actually remove these distractions and
introduce more zone times, thus more productivity. So how can you make this
work?

##1. Commitment

There has to be a commitment, not just on the immediate team, but from the
executives on down to having successful remote employees. No matter how
dedicated a manager is to having the truly best players regardless of location,
if someone up the chain isn't 100% behind the idea of remote workers, they will
poison the whole thing.

It will likely start slow and unnoticed, but it does slowly creep its way in.

##2. Communication

Since you are working from home, it becomes harder to look over your shoulder, across
the aisle or walk around the corner to ask a question. The people just aren't
that close. Communication needs to find a channel where people can talk, not
just one-to-one in IM, but in a team setting. Barriers need to be sought out to
lower the friction of a quick check with a coworker to solve a problem.

This starts with a quality group chat application. It could be as simple as
using IRC on Freenode, or using tools like Slack, FlowDock, and HipChat. These permit the
one-to-one communication, but are really built to replace the shouting over
desks that happens in the office. 

When using your chat application, each team member needs to communicate with the
rest of the team when they are available. There will be times when you need to
log off chat, be it to get lunch, take a meeting, run an errand, but most likely
to create a zone state. Instead of just dropping off or logging out, send a
message first. Let your team know you are dropping off, why, and how long you
expect to be. Greet them in the morning and say goodnight before you leave for
the night.

This little courtesy will go a long way. The better you are at it, the more time
you can take to be in the zone. If you have hours that you work the best
during, block those out on your calendar & show yourself as busy. When those
roll around, log off and focus.

Whenever possible, step up the communication from text to voice or better yet
video. Daily standups or check-ins should be over video. Any meeting as well.
Find a tool where you can screen share, like Hangouts, Slack, HipChat, or Webex.
Whenever the discussion is not just social and more than a few quick volleys of
text questions and answers, jump into a video chat. Let people see your face.
Share screens and collaborate on the project, paid programming, design
critiques, or joint editing all bond the team players and actually produce
better results faster than passing them back and forth in email.

##3. Social Inclusion

A hard thing to deal with as a remote worker is the lack of water cooler talk.
The average worker spends 40 plus hours a week at work and an additional 10
commuting. If you sleep 8 hours a night, then you family and social time is
reduced to 62 hours, and half of those are your weekend. If you're like me, they
get consumed running errands and doing work around the house. There isn't much
time to see friends or family.

This is why having social relationships with your workmates is important. With
the amount of time we spend in work, we need to get along and share experiences.
As a remote worker though, you are often working at home, alone. Having a
communication with the team that is less formal than the rest of your meetings
builds this bond. Set up an "Off Topic" room in chat where people can be
themselves and let loose with the gifs, YouTube videos, and jokes.

One of the more successful teams I've worked with would do daily standups via
Hangout, but every Friday after standup they would have a "Show & Tell". One
team member would put together a presentation, preferably not related to work,
and share with the team. This gave everyone a chance to really get to know who
they are and what they enjoy. I learned how to make a roux, how to grow hops and
even a bit about hockey. 

If your team is split remote and in the office, make sure to only throw team
events that can be shared by everyone. This doesn't mean you can't go out and
have dinner or get drinks after work to celebrate, but make sure to create an
event where the remote teams are able to participate.

Needs some ideas of how to include remote workers in a celebration event? Try
some of these:

- Send them a meal! Call up a local restaurant they like, order their favorite
  dish and have it delivered when the rest of the team is having lunch. Make
  sure to put them up on video chat in the meeting room so everyone can talk.
- Movie night! Find a campy or favorite movie on Netflix & set up an audio chat.
  Have everyone on the call start the movie at the same time and enjoy a little
  MST3K action with team by snarking, sharing trivia, or playing games based on
  the flick.
- Game night! There are ways to play a variety of games online or over video.
  Great examples are Magic the Gathering, Dungeons and Dragons (checkout [Roll20](http://roll20.net)), Pictionary (Using Scoot & Doodle),
  or Trivial Pursuit.
- Cooking lessons! Get a volunteer to teach how they make their favorite
  recipe. Send out the list of ingredients a few days before hand so all the
  participants can get them, then teach from the kitchen while everyone cooks.
  Rotate out the chef so everyone can be the teacher.
- Book club! Pick a book, set a time, get some wine and discuss over video. I
  participated in a remote sci-fi book club that met monthly to discuss the
  book. We had dozens of folks turning up on the chat. I've also done a tech
  book club, where we would read a couple of chapters a week and discuss how to
  implement these in our own work.

##4. Eliminate Distractions

Now that we eliminated the problems of going remote and staying in tune with the
team, there are two things prevent remote workers from being productive at home.
Good remote workers know how to limit distractions. They also know how to
balance their time. When the you don't have to leave the building, it becomes
easy to get drawn back into work.

Set a routine up. Every morning I get up with my kids, get them out the door to
school and feed the cats. Then I shower. Sure, I could work in my PJs, or even
without pants, but by showering, I'm setting a stake in the ground saying, "My
day starts now."

I'm not good at taking breaks. So I started using the Pomodoro technique. I set
a timer for 25 minutes. During that time I work. No Twitter, Facebook or
Youtube. I'll mute chat and email. When the 25 minutes are up, I'll take a ten
minute break to do some stretching, get a drink or food, head to the restroom.
When I come back, I start another 25 minutes. Every few work slots I dedicate to
catching up on chat, email, or any other communications I need.

I also have an alarm set to remind me to eat lunch. If I get in a flow, or worse
and have a bunch of meetings in a row, I forget to eat. Taking care of yourself
first is the most important thing when working remote. I have another alarm set
to end my day. Yes, occasionally I run over, but I try to keep my work day under
10 hours. When I hit that, I shut down anything work related, help my family
with dinner prep, and enjoy a nice evening with them.

If you have a problem staying off social media, set filters on your hosts file or
your router that prevent you from going to those sites during work hours.

Remember you are working. Don't do house chores unless you are on your 10 minute
break. And if you have little kids or pets, get a door and close it.

One more thing I try to do daily is go out for a walk. It's hard in the
Wisconsin winter, especially for someone who is already unstable, but getting
out for 20-30 minutes can really refresh you brain and your body.

Last piece of advice I have for remote workers, find an excuse to leave the
house for at least an hour every couple of days. It's best if you can meet
with friends. Just don't let yourself become accidentally agoraphobic.
