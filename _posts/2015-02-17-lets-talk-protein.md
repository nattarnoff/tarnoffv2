---
layout: post
title: "Let's Talk Protein"
comments: false
date: 2015-02-17 08:45
categories: [Health]
---

I have been a vegetarian or vegan for a nearly 10 years. I first chose to
be a vegetarian after having read The China Study. My family has a history of
heart disease and cancer, and I was following the same path as my father who had
a heart attack at 41. The China Study claimed that animal based protein was
a major catalyst in these &ldquo;western&rdquo; diseases. In hopes to avoid my
father&rsquo;s path, I embraced this new diet. It wasn&rsquo;t difficult as I
didn&rsquo;t eat much meat
and it was mostly chicken.

<!-- more -->

I took the diet seriously and quickly lost some weight and began feeling
healthier. One thing that didn&rsquo;t change were the headaches I had been suffering
for several years. I had started having daily migraines a little over a decade ago. At first they came in waves for a few weeks, but eventually never went away. 

Going vegetarian was simple. Finding adequate protein was more challenging.
Initially I went vegetarian before researching what I needed to be eating. I
thought it was as simple as removing things. Eventually I learned I needed to
focus on certain foods in order to have a balanced diet and began including more
things nuts, legumes, tofu, &amp; tempeh to find a dietary equilibrium. Once I
had this down, getting enough protein wasn&rsquo;t an issue.

I experimented with varying degrees of vegetarianism during the early years
based. For two years I was completely vegan. During that time I was even raw
vegan (not heating anything over 108&deg;F&mdash;usually only for dehydrating)
and doing green smoothies. Both of those left me feeling empty as I just
couldn&rsquo;t consume enough food without carbs like pasta to stay full. I dropped a
bunch of weight quickly, but felt lethargic.

Just over two years ago, one of my headache symptoms began showing up on its
own, vertigo. In just under six months I went from being a fully functioning
person to someone who couldn&rsquo;t drive, walk more then a few hundred yards
unassisted, clean my home, or enjoy a trip to the theater with my family. While
I can better deal with the symptoms now, I still suffer severe dizziness every
day.

One of the issues I face on a daily basis is a drop in energy when a spike in
vertigo happens. I can literally feel the energy flow down my body and out my
feet. so far there have only been two solutions to this&mdash;eat something or
sleep. Most of the time this hits, I am out with family, so taking a nap is
difficult. Because of this, I carry a handful of Larabars with me wherever I go.
consisting of fruit and nuts that are all raw, they pack a pretty good, natural
boost to get me through the next little bit. However, then never bring me back
fully. I needed something with a bigger protein boost than the snacks available
to me could provide.

About 2 months ago I began eating a few bites of my partner&rsquo;s meals, primarily
chicken, to see if my body can handle it. The last time I had chicken I had
become violently ill. I&rsquo;ve slowly built up a tolerance for meat again and have
reintroduced it into my diet. I am no longer vegetarian. Sometimes this is
difficult for me to cope with. However, I find that when I consume meat in at
least three meals a week I lose less energy and stay mentally sharper when
working. I still have limits. I can&rsquo;t go running (walking a mile still drains
me) or work long hours without a significant loss of productivity the next day,
but if just getting through the day is eaiser and finds me producing more high
quality work, it seems to be worth it.

This was a hard decision. Despite my first turnign to vegetarianism for health
reasons, I did end up exploring how our food is produced and developed a very
large heart for the animals we use for sustenance. I have become an advocate for
animal rights and feel that these creatures are far smarter than we credit them
and should be treated more as equals than the slaves they are. Simply though, in
order for me to be productive enough to support my family, I needed to make this
decision. 

At this point, the best I can do is to only purchase local, family farm raised,
cage free meat. Getting it from places where they might have names instead of
numbers, are treated with respect and not force fed is the closest thing to my
animal advocacy I can get. 
