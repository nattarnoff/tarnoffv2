---
layout: post
date: 2015-07-30 10:00
section: blog
title: "I'm on CtrlClickCast Podcast Episode 49"
categories: accessibility
---

Last fall I met the great hosts of [CtrlClickCast](http://ctrlclickcast.com) at
[CSSDevConf](http://twitter.com/cssdevconf) and they were gracious enough to
invite me on the show. I got together with Lea & Emily last week and recorded an
episode on Accessibility. It was super fun! I hope to do more podcasts (but I
need a better mic!). 

<!-- more -->

<img src="/img/ctrlclick_200x200.png" alt="CtrlClickCast" class="right" />

The episode is now [live](http://ctrlclickcast.com/episodes/accessibility-101)! Go listen to it. Really, right now. I'll wait. It's
about 50 minutes long and Emily and Lea have had the show nominated for the Net
Awards recently, it's a really great show.

Here is a direct link to the [full transcript](http://ctrlclickcast.com/mint/pepper/tillkruess/downloads/tracker.php?url=http://cdn.ctrlclickcast.com/transcripts/2015/ctrlclickcast-049.pdf&remote). 

I'm not embedding the podcast here because I want you to go to [iTunes](https://itunes.apple.com/us/podcast/ctrl+click-cast/id446900959) or
[Stitcher](http://www.stitcher.com/podcast/ctrlclick-cast?refid=stpr) and subscribe. And after you listen, give a review!
