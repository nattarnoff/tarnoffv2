---
layout: post
date: 2015-07-07 10:00
section: blog
title: "On Depression"
categories: Health
---

Society has many ills. People are fallible and get themselves addicted to unhealthy things all the time. It may be overeating, under-eating, gambling, alcohol, or drugs, but each of these people is dealing with a pain that their particular addiction gives a little bit of relief from. We recognize that these addictions are diseases; we spend millions a year advertising and partaking in various treatments for them. We recognize that people who have these addictions need to take life a day at a time; there is no magic wand for addiction. **It is a constant struggle**. But many of these addictions are just covering what the real problem is, mental health. As a society we don’t talk about mental illness. We don’t admit that is too is a disease that takes a daily fight and this needs to change.

<!-- more -->

## Trigger Warning - I say some things some folks won't like

<img src="/img/semi-colon-tattoo.jpg" alt="My story doesn't end here;
It's just beginning - tattoo by @kittencaboodles" class="right" />

I’m sure you’ve heard it before, "Depression is hell". Depression is a deep dark cavern that makes it hard to see the good happening around you. It can be all consuming and weigh you down like an anchor. We look for the light, but too often we can’t see it. But everyone experiences their depression a little differently. I want to describe my depression, because I’ve experienced a lot of it, and it gets reflected in everything I do in one way or another. I can’t speak to all mental health issues, I can only speak to mine. Hopefully by sharing, you will learn what I deal with. Maybe you can relate, maybe you can’t. What matters though is that you take me seriously, as well as all others with mental health issues. Respect them and don’t judge them. 

## Emotional Heart vs. Rational Mind

I tend to think of myself as a rational person. I have a degree in philosophy and enjoy a hearty debate. I excel in logic problems and write code for a living. I work well through puzzles, yet I am at the same time quite emotional. Every moment of every day I have to make a decision on whether to listen to my emotional heart or my rational mind and they are usually in conflict.

## The Cycles of My Depression

### The Good

When things are good, like when I fall in love, it becomes very hard to discern between my heart and mind. They want the same thing. My heart is a solid rocket booster burning furiously in just a few moments, while my mind is the long lasting candle you are glad you packed for the tornado kit. I have to choose, and usually fail, to slow things down and take one step at a time. Since both heart and mind want the same thing, the heart usually wins and I get consumed in the flames. Typically, this means something else gets ignored. My health, house, family, or friends are the most likely culprits. If I could follow my mind a little more, I could have it all.

### The Bad

When things are bad, and they usually are bad, it is a lot easier to see, but harder to follow the right path. The dark thoughts range from worry about money, weather, or health to unidentifiable panic and self-harm. Very few of these thoughts deserve any merit. Most are non-sensical to begin with and can be quickly forgotten when I’m healthy. A few, like worries about money, are legitimate concerns exaggerated by my unhealthy thoughts. When carefully examined, they are easily dealt with. And while I say I can forget, reason, or ignore things, what often happens is I clench onto them like a kraken around a whale. It’s this grasping and holding that turns into the depressive spiral. I get tunnel vision and only see this one problem, ignorant to all the good in my surroundings.

I tend to be a hypochondriac, which sucks, but sucks even more when you have actual health problems without solutions and cures from doctors. I also get really lonely. I have a full house, two kids and three cats, and lots of people who care about me, but that doesn’t change the fact I get lonely. In the flick of a switch, I can feel like the last person on the planet, while having a conversation with someone sitting right in front of me. Working from home and only interacting with people in a digital realm doesn’t make this easier, but my physical health precludes me from working in an office every day.

### The Ugly

I think about suicide a lot. I have since I was a kid. I’ve tried to take my life several times in the past. **I plan not to do it again.** This is a very important statement. It isn’t that I don’t plan to kill myself, but I specifically plan **NOT** to commit suicide. **I have no desire to die.** I want to live, watch my kids grow up, travel the world, help people build better technology, and make beautiful art. I want to climb mountains, hang out with my brother, sister-in-law, nice and nephews, maybe even relearn to surf. It doesn’t matter though. Every day, sometimes every hour, sometimes every minute, I have a thought of suicide. When I’m having a bad day, especially if I’m in a lot of pain, I am in a constant struggle of having a suicidal thought and letting it go. Medication only takes the edge off so that I win these battles more than lose.

## How I navigate the day

I use meditation as a practice to learn to let go thoughts. Focus on the breathing. When a thought comes in on something other than my breath, acknowledge it and let it go. This takes focused effort. When I’m depressed they don’t just go away. My heart requires justification before it lets go. This means half my time and mental energy is spent debating my inner self, just to keep moving forward. Think about that. Think about how much time you spend on solving a problem and then try to solve that same problem if every other second you had to argue with someone about a completely pointless and random thing. It becomes really hard to get into a flow space for solving hard problems with this going on. 

Meditation for me, and while I know a lot of folks who have had it work for them, it will not work for everyone. I still get into a funk periodically, but most days are better now. I know I can ride out the dark times, but many people with depression can’t.

## “Just Get Over It"

Depression isn’t something to “just get over” for most people. Don’t say this to anyone. It shows a clear lack of understanding of what they are going through and a lack of compassion for them as an individual.

Depression is a daily struggle. Some of us have the upper hand on our depression right now, but I can assure that many will eventually slip, needing more understanding, and maybe some help some time in the future. Others are in the heat of the battle and are losing. They can’t break the cycle of bad thoughts. They need understanding, patience, and when they ask for it, help. You can’t force someone into accepting help, so don’t even try. No matter what you do, if they aren’t ready for it, it won’t stick. This doesn’t mean you shouldn’t call for help if they are hurting themselves or others. There is emergency help and long term help. If a person is a threat, emergency services need to be brought in, no questions.

## How You can Help

**One in five people struggle with mental illness every year.** Twenty percent. More people suffer with mental illness than smoke cigarettes. This isn’t some small group we can cast out. These are your neighbors, friends, lovers, parents, cousins and children. Break the stigma and taboo. Talk about it openly and acknowledge that it is a disease that people struggle with daily. More people have mental health issues than breast cancer every year, yet we talk about it less and spend less money fighting it. It is up to you to be compassionate, patient, offer an ear when you can, but always give someone suffering from mental health the same respect as anyone else.


