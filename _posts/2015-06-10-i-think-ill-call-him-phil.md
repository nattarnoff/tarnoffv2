---
layout: post
date: 2015-06-12 12:30
section: blog
categories: health
title: "I Think I'll Call Him Phil"
---

It always starts at my cheek. It crawls behind my eyes and tugs on my nose. 
Unmonitored, it will push tears out then start chipping away at my teeth. 
It threads a needle through my left eye and pulls on the string making me lose focus and balance. 

<!-- more -->

<img src="/img/dreary-day.jpg" alt="Dreary day of grey skies and greay snow." />

I can feel it tickle its way across my cheekbone and dig into my ear. 
It binds up the muscle in my neck with its own special form of a bear hug. Hanging by my shoulders is burrows into my 
chest and kicks at my spine. From here it grabs the nerves of my limbs like 
Pinocchio’s strings. My joints start to buckle. My fingers become devilish claws
 preventing me from typing or holding anything.

<img src="/img/trees-in-snow.jpg" alt="bare trees in snow reflecting the gloom
of pain" class="right" />

Knocking begins on the base of my skull. I feel it crack as its worming between 
my brain and skull continue. It turns my grey matter to goo and pushes it out 
to the edges. Words go missing, thoughts disappear, and my focus dissolves 
into tiny spheres of water vapor. It pulls the muscles in my shoulders and ties them onto my ears.


Grabbing my optic nerves, it forces my eyes to find the brightest lights 
starting the cycle over. It gives me no choice, all sound makes me scream,
 but if I don’t flood my head with music, I just hear the banshee shriek all day, every day.

This is my pain. I think I’ll call it Phil. Phil never sleeps.
