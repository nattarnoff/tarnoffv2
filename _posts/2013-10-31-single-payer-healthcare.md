---
layout: post
title: "Single Payer Healthcare"
date: 2013-10-31 10:56
comments: false
categories: [Equality]
---

I'm pretty lucky. I have a solid career and reliable income. I also have health insurance through my employer, unlike millions of other Americans. However, something happened this week that scared the hell out of me. I caught pneumonia.

<!-- more -->

## This shouldn't be that scary

Pneumonia is fairly common and easily treatable with a dose of antibiotics. Sometimes it means a short stay in the hospital. Why should I worry? I panicked because it was October 30th. I just changed jobs and the new insurance doesn't kick in until November 1st and I didn't have a new primary care provider lined up yet. Yes, I still had coverage until the end of the month. Also, I could have extended Cobra insurance for the month of November to cover any bills.

Let's look at this though. I didn't have to worry about coverage. I had that by numerous angles. But in order for it to work I was going to have to pay double insurance for a month at quite high rates (nearly $1000) or spend hours on the phone with insurance companies figuring out who covers what and what my out of pocket comes to. No one should have to do this.

We talk about the uninsured all the time. We talk about the young and how group plans will go down if they are in the pool. We don't talk about those of us who are transitioning insurances. It could very easily be that my new insurance wouldn't cover a followup or hospital stay as of the 1st. Single payer health insurance makes this a non-issue. Nobody has to worry about medical bills, thus making recuperation easier and faster as they don't have the stress of the financial burden hanging over their head.
