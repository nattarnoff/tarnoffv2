---
layout: post
title: 'A Word on Color'
date: 2015-03-02 09:45
comments: false
categories: [Accessibility]
---

Over the weekend, there has been a lot of buzz about &lsquo;the dress&rsquo; and
whether it is blue and black, or white and gold. The reality of the dress is
that due to white balance in the camera, lighting conditions and the fact that
everybody perceives color a little differently the true color of the dress can
not be known from that single photo. But it is this last piece I want to talk
about today.

<!-- more -->

##We All See Color Differently

Business Insider decided to comment on the dress by publishing an article titled
["No one could see the color blue until modern
times"](http://www.businessinsider.com/what-is-blue-and-how-do-we-see-color-2015-2).
Conducting a test amongst a tribe from Namibia that has no color for blue,

>When shown a circle with 11 green squares and one blue, they couldn't pick out
>which one was different from the others ... But the Himba have more words for types of green than we do in English.
>...When looking at a circle of green squares with only one slightly different shade, they could immediately spot the different one.

If we don't have a word for a color we struggle to identify it. Additionally, it
has been shown that some people see more colors than others. Men often struggle
to see the slight gradiations between colors in women's clothing. If a woman
bought a new blouse that was Robin's Egg blue, her husband will likely just see
it as light blue not discerning the difference between the two shades.

Colors are tied to our language. As a collective we have decided that the sky is
blue, however our biology isn't as decisive. Some will see light blue, others
dark blue, and some see a bluish-white color. This is due to the way our eyes
are built. 

##Not All Cones are Ice Cream Cones

Our eyes posess cones that absorb light and sends the signals down the optical
nerve to our brain to process into images. The images hit our cones inverted
because of lenses work. Our brain is already correcting for that, so let's keep
the software in mind as we discuss the rest of the system.

Most people have three cones, one each for the red, green and blue channels.
This is very similar to how TVs are made. The pixels in the screen have three
channels, red, blue, and green. They give off light and we capture it using a
mirrored system. However, not everybody can see these three distinct channels. 

We know some folks are color blind and struggle with colors of the same shade or
greens and reds depending on their type of color blindness. One in seven men
suffer from this. However, some folks aren't technically color blind and yet
can't see as many colors as others.

<figure>
<img src='/img/colors.png' alt='rainbow of 39 colors for testing vision' />
<figcaption>From <a href="https://www.linkedin.com/pulse/25-people-have-4th-cone-see-colors-p-prof-diana-derval">Derval
Research</a></figcaption>
</figure>

This image displays a rainbow of colors. Some people can see around 20. They are
missing a channel on their cones and relying on just two channels, similar to
dogs. Some people see in excess of 35 (I see 39) and have a 4th cone. These people are called
[tetrachromats](http://en.wikipedia.org/wiki/Tetrachromacy). Birds are also
tetrachromats. They have cones that are sensative to ultraviolet light. 

##Web Designers Need to be Aware
As designers and developers, hopefully we are aware of putting together designs
that the colorblind can use. You can test that with a number of tools like,
[Colorable](http://jxnblk.com/colorable/demos/text/) or
[WAVE](http://wave.webaim.org/). 

However there are other color patterns that can hurt people and make your
content and site difficult to navigate and read. 

###Not so Black and White

One combination is the classic black and white. Using pure colors can be
difficult on a number of people. Some will need to turn down the brightness on
their monitor (me), some will actually feel phyisical pain (common for autistic
people). Simpy toning down the purity of the colors makes it more manageable.
For instance, using <code>#333</code> instead of <code>#000</code> will drive a
vast improvement in the readability of your content. You can go one step further
and pull back from pure white to <code>#fefefe</code>.

But black and white aren't the only ones who cause a problem. Bright neon colors
are a recent trend, but reading text on these backgrounds or hafing neone text
on a neon bakground will trigger physical pain for users.

The 2015 [CSSConf](http://2015.cssconf.com/) just rolled out the new site for
their conference in NYC this year. they have a beautiful, hot trendy neon
design. But is it easy to read? Well that depends on your cones. If you have two
or three channels, it might work. If you are a tetrachromat, the push to neon
colors can be painful when put together or reading long text lines. Additonally,
some of these colors don't pass the WCAG 2.0 AA guidleine for contrast (which
should be our minimum goal).

![white text on pink background](/img/cssconf1.png)
![blue text on a neon blue background](/img/cssconf2.png)
![Pink text on a neon blue bakcground](/img/cssconf3.png)

When colors vibrate next to each other, they don't make for good reading. Let's
take the time to cartefully select our colors and test them with our target
audience. Part of that testing should include asking the user what color they
see and to describe it in detail. Color is important, so let's work hard to make
sure all our users are seeing what we want them to see in a pain free way.
