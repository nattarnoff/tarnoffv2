---
layout: post
title: "Spinach Linguini with Pesto and Avocado"
date: 2013-04-07 13:29
comments: false
categories: [Cooking]
---
This weekend hoofin' it around the Capitol Square farmers market, I found a booth selling fresh handmade organic pasta. They only had two types that were vegan, so I bought a 9 oz package of their spinach linguini to use for dinner tonight. Of course, I hadn’t figured out what I was going to do with it yet.

<!-- more -->

This morning, watching the fog as the sun rose, I looked out onto the porch and saw that the basil needed to picked and used. Bingo! I am making pesto. So here is is the recipe:

##Ingredients

- 1-2 cups packed fresh basil leaves
- 1/2 cup Extra virgin olive oil
- 1/4 cup pine nuts
- 4-5 cloves fresh garlic
- 1 cup halved grape tomatoes
- 1/2 avacado diced
- Pasta

In a small processor combine the basil, garlic, oil and pine nuts and grind/blend until you have a consistency you like. I prefer the nuts a little chunky (not sure why, I like creamy nut butter) so I add them last. Modify the oil amount for a consistancy you like as well.

Cook the pasta per directions. Once cooked, rinse with cold water to stop the cooking and cool the pasta. Mix the pasta with the pesto and tomatoes and set aside. Tonight I made a lite salad to go along with it at this point while I let the pasta cool to room temperature.

Plate the pasta and any sides you are serving, then sprinkle the avacado on top to taste. If you have no aversion to salt, a little sea salt will go well with the avacado and pesto. Serve and enjoy!
