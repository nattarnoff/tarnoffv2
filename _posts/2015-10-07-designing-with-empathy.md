---
layout: post
date: 2015-10-07 10:00
section: blog
title: "Designing with Empathy"
---
This weekend I had the pleasure of speaking at the inaugural edition of Open
Source & Feelings. It was an amazing conference tackling some really hard
topics. I received really great feedback from the audience on my "Designing with
Empathy" talk and several asked for the transcript as they couldn't take notes
fast enough. So here is the talk broken out with what was on the slides as well
as the script I tried to follow. The video is coming, and I will add that when
available.

<!-- more -->


When I first pitched this talk, I was thinking it was about accessibility because that's usually what I speak about. As I wrote it. And rewrote it. And rewrote it. It evolved into being something bigger, higher level, and more important than just accessibility. It became about thinking of others and building experiences that we can all be proud of.


---

# Trigger Warning

### Some slides contain content that may trigger motion sickness

### #DZY 

[tarnoff.info/blog/granting-the-user-control](http://tarnoff.info/blog/granting-the-user-control)

Some of my slides contain motion that could make you feel ill. I'll give you a
warning before they occur. Additionally, I'm promoting the hashtag #DZY which
you can read more about at this [link](http://tarnoff.info/blog/granting-the-user-control).

---

# Trigger Warning

### This talk discusses divorce, death, & trauma

In talking about empathy, sometimes we need to talk about bad things. I use
some examples in my deck of bad design decisions, and some of this are based
upon hard topics. I will provide additional warning before those sections.

---

# Designing With Empathy

Technology should be the great equalizer. It should provide access to things that may never have been available to us before. True innovation raises the quality of life for all people granting them increased independence socially, physically, mentally, and financially. But if we aren’t diligent in our design; if we lack empathy while we build tools, we leave all of that behind. To get us started, I want to set a common language.

---

# Art vs. Design

Art and design have a rectangle/square relationship. While seeming quite similar, they are not the same. Art is an expression of the creator that MAY communicate a message. Design must answer a question. Design should do this in the most simple and elegant way possible. Design must communicate a message and solve a problem. Mariona Lopez says, “Design is a formal response to a strategic question."

---

>Great design is all the work you don’t ask the people using your product to do. -Rebekah Cox

Design is not always visible. We design databases, algorithms, and experiences. As designers our goal is to empower all of our users to be better versions of themselves. We can do this by providing tools and experiences that make them more efficient, more engaged, and happier contributors to their communities.

---

# What is Empathy?

## The ability to understand & share the feelings of another

![Hulk & Kermit fist bump, "I know that feel bro"](/img/being-green.jpg)

Empathy is listening to others. Putting ourselves in their shoes. Trying as
hard as we can to see the world the way they see it. Empathy is the sharing of
our feelings, emotions, and experiences to make us deeper, richer individuals
and a better society.

---

# 80/20 rule

80% of the activity or revenue for the things we build will typically rise from 20% of the users. They are power users. They know the system in and out. In all likelihood they helped you design the solution in the first place. They know it as well as you do. It is tempting to continue to build tools for them as it has a very visible ROI. And this is the problem. 

---

# Confirmation Bias

By our nature, we surround ourselves with people who we share things in common with. It makes us feel safe and secure. It doesn't challenge us, and that feels good. Empathetic design acknowledges this. We have a bias and we seek out people that will not be “yes” people in order break that bias.

---


>We can’t expect men in startups in SoHo to solve the problems of people in the South Bronx or rural Nepal." -Stephanie Morillo

![Stephanie Morillo](/img/stephanie.jpg)

Empathetic design works with people to solve problems. 

Empathetic design doesn’t solve problems for people; but rather with people. Let them tell you what the problem is and in return find the solutions together. If the solution isn’t inclusive, it isn’t a complete or empathetic solution.

---

# My Design Process:

You probably would like to know how I make sure I design with empathy in my
work.

---

# Interview ➞  Ideate ➞ Validate ➞  Repeat 

Empathetic design always starts with interviews. Interview users and target
audiences. But empathic design involves interviewing users who aren’t like you.
Break your confirmation bias.

Empathetic design validates every idea it puts forward. 

Empathetic design iterates to continue refining the solution. We are
human and won't get things 100% right the first time.

---

#People of Color, Women, Transgender, Non-Binary, Homosexual, Asexual, Bisexual, Urban, Rural, Physically Disabled, Neurologically Diverse, Vision or Hearing Impaired, Mental Illness, Refugee, Literacy Challenged, Economically Challenged, Assault Survivor, Age, Religion, Culture, Addiction, Trauma, Access to Technology & Basic Needs 

So who do we interview? Many of us will fall into a one or more of these communities. Mark those represented on your team, and come back to them later.

Start with 3 people that don’t share at least one category with you, your stakeholder, or the other people you are interviewing. During each iteration, add people to the interview list and focus on adding categories. 

---

# Design Rules!

These are the things I try to think about when
I'm working on a project.

---

# Content First

If you don’t know what content is going into your solution before you build it, you don’t understand what you are making. You need to compose the content before you begin ideating. It's ok if the content isn't final. we iterate code and design, we can iterate the content as well.

---

![Startup bragging about expensive cars](/img/Sorry-dave.png) 

via @SaraSoueidan

Empathetic design doesn’t contain jokes only you and your bros find funny. This is a sure way to drive away customers.

---

![Wanted Dead or Alive](/img/wanted.png)

Empathetic design doesn’t include completely irrelevant choices that could possibly come across as disturbing.

--- 

# Communicate Clearly

---

![Light text on dark and light backgrounds](/img/contrast.jpg)

Empathetic design makes your content easy to read.

---

![Pre-Overdue Notice](/img/overdue.png)

Empathetic design uses language that a 13 year old can understand as this is the average reading level. Empathetic design will be internationalized so that each user can work in the language they are most comfortable in.

---

![Someone typing, but session is ended.](/img/end-session.png)

Empathetic design doesn’t send mixed messages of “someone typing” and “session ended” at the same time.

---

![Amy Hoy](/img/amy-hoy.jpg)

>There *ARE* design choices that *CAN* harm a user. -Amy Hoy

---

# Which of these scream “click me”?

<img alt="sample buttons" src="/img/buttons.png" class="dark"/>

Empathetic design knows that if we provide small affordances, the user will have an easier time using the product. Minimalism for minimalism’s sake produces a higher cognitive load in a user experience.

---

# Do No Harm

Empathetic design goes out of its way to think of the edge cases to make sure it doesn’t cause harm to the user.

---

# Trigger Warning

## The next few slide mention divorce & death

These next few slides mention design decisions that triggered trauma in their
users due to divorce or deaths they experienced in their lives.

---

![Ads targeting people going through divorce on faceboo](/img/dave-strock.jpg)

>Yes, Facebook, I'm getting divorced. Now please kindly piss off. -Dave Strock

Facebook often suggests my child’s therapist as a “friend”. But that isn’t as bad as what Dave has been getting lately. Because he's posted about getting divorced, advertisers are using those words to target him. This ad experienced was designed by someone.

---

![Would you like to delete Sam Barnard](/img/delete-people.png)

>If the dialog involves a human always ask how it reads if the person is dead, estranged, etc. -David Barnard

Empathetic design checks the language for a variety of uses in case the user enters non-standard or unexpected data. 

Sam is David’s brother and passed away unexpectedly on Feb 4, 2015. Sam and David stood in line for the first iPhone and each one thereafter. Sam helped David with his iOS development and testing.

---

![Year in Review showing Eric's daughter](/img/eric-meyer.png)

>When you say edge case you’re really just defining the limits of what you care about. -Eric Meyer

Empathetic design knows that a user may have had a bad year and they don’t want to be reminded. 

Eric lost his daughter to cancer. When the “Year in Review” from Facebook came up, he ignored it. He was still grieving and knew what it would show. Then one day, Facebook trying urge him to use it, published this with her image. While forcing a user into something is never a smart idea, simply not using her photo would have saved him the grief he felt that day.

---

# Do Not Assume

Empathetic design doesn’t assume anything about it’s user. Designers ask before building systems, during user setup, or they leave it out. 

---

<img src="/img/icons.png" class="dark" alt="sample icons" />

Empathetic design knows that your icons don’t mean the same thing to everyone. Icons are societal based, skeuomorphic images that not all people have been exposed to. Empathetic design uses icons in conjunction with words to avoid confusion.

---

![Spuose is same gender](/img/spouse.jpg)

via @ohambiguity

Empathetic design doesn’t care about the gender of your spouse. 

---

# Do Not Ask

Empathetic design doesn’t ask for personally identifiable information if it isn’t absolutely required. Empathetic designers do challenge stakeholders when they ask for more demographic information on their users.

---

![Facebook's HQ sign celebrating Pride](/img/facebook.jpg)

Empathetic design lets the user specify their name without having to pay a penalty for it, unless it is legally required.

---

![OKCupid's Gender options](/img/gender.png)

Empathetic design knows that gender, sexuality, and race are vast and confusing. People may not be one thing versus another, they can be combinations. Empathetic design lets the user be themselves without adding to or creating stress and anxiety.

---

# Guided Workflows

Empathetic design makes it extremely difficult to make mistakes.

---

![Jared Ficklin Error](/img/ficklin.png)

Name: Jared Ficklin

Error: Choose a name that doesn’t include profanity
Regex: (f|F)(\w|@|#|!|$|%|^|&|\*)(ck)

Empathetic design knows that a user’s name could be anything and therefore can’t throw an error.

---

![Being told Arab letters aren't letters](/img/arabic-flight.png)

Empathetic design lets you use your native language to fill out forms.
Empathetic design knows that not all people in English speaking countries are speaking
English.

---

# User Control

Empathetic design gives the user control of their experience. Empathetic design gets out of the way of users with assistive technology and lets them have complete power over the solution. 

---

![Collection of devices](/img/devices.jpg)

via @bradfrost

Empathetic design knows that not all users have mice and allows them to access everything with the devices they have. This includes not just touch devices, but assistive technology: Screen readers, braille displays, closed captioning, eye tracking, or natual language interfaces.

---

# Trigger Warning

## The next slide contains motion

## #DZY

---

![Sample collapsing menu](/img/collapsing-menu.gif)

Empathetic design learns the user. If the user doesn’t need the navigation labels, they can have just the icons. Empathetic design knows that if the user collapses the navigation, to keep it collapsed next time they come in.

---

# Trigger Warning

## The next slide contains motion

## #DZY

---

![Sample of 3D Touch by Apple](/img/3DTouch.gif)

Empathetic design doesn’t make the user ill with fancy animation just for
animations sake. Empathetic design lets the user control things that may trigger
unpleasant reactions.

---

# Progressive Enhancement

Empathetic design still works on the oldest technology and adds features as the technology gets better. Just last week I sat next to a woman who uses a pink Motorola Razor and she told me she does go online with it.

---

# Leave No User Behind

Empathetic Design leaves no users behind. Empathetic design works for anyone who chooses to try.

---

![Chalize Theron as Furiosa](/img/furiosa.jpg)

>When building products, focus on making the user feel badass -Kathy Sierra

Empathetic design makes badass users.

---
