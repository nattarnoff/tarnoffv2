---
layout: post
title: "I Love Retro Cameras"
date: 2011-08-03 13:00
comments: false
categories: [Photography]
---
I have a collection of 18 cameras (for now) ranging in manufacture date from 1936 through the 2010. I have twin lens reflex, medium format, Polaroid, range finders, folding, 35mm and digital cameras. I can’t get enough. And I still shoot film. The problem is the scanning and post processing of the film.

<!-- more -->

“So shoot digital!” people say. Well I can’t give over that much control, namely focus and depth of field, to a digital camera and I can’t carry my DSLR everywhere as I never leave the house with less than 2 lenses.

Finally though, it looks like Fuji has made a camera for me. Introducing the X100. Retro range finder looks, modern 12.3 megapixel APSC sensor. It also shoots 720P video. the best part? Compact, lightweight, manual settings, f2 35mm (35mm equivalent) fixed lens. No carrying multiple lenses, no excuse not to have a camera. And it has personality. I am now saving up for this. So if you have a shoot you need shot, or a website worked on, or want to donate and help me acheive this small dream, just let me know. Oh, I am selling my Orbea Fleche (2009, 52cm) road bike to start the fund if you are interested.

Here is a good “photographer’s” review (not heavy on tech specs) at [Zacharias.com](http://Zacharias.com).
