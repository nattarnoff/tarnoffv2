---
layout: post
title: "Blazin’ Three Bean Chili"
date: 2013-04-07 13:30
comments: false
categories: [Cooking]
---
This one comes with a warning. If you make this recipe as specified below, you better like hot foods. This what I call a triple burner, once on the way in, once while digesting and once on the way out. Now, not everyone feels hot foods the same way, but if anything I make is a triple burner, this is it. Feel free to modify your recipe to eliminate some of the peppers and spices until you get it to the heat you like. When I use hot peppers, I never take the seeds out. This is where all your flavor really comes from. If you find the peppers to be too hot, reduce the amount used instead of removing the seeds.

<!-- more -->

## Ingredients

- 1 can (150z) or two cups cooked red kidney beans
- 1 can (15oz) or two cups cooked black beans
-1 can (15oz) or two cups cooked pinto or white beans
- 1 can (28oz) or 4 large diced tomatoes
- 1 jalapeno peppers
- 2 serrano peppers
- 1 red onion
- 2 ribs of celery
- 1 green pepper
- 1 red pepper
- 3 cloves garlic
- 1 tsp coriander
- 1 tsp turmeric
- 1 tsp chili powder
- 1 tsp cayenne powder
- 1 tsp crushed red pepper
- 2 cups water
- 1 cup of quinoa (uncooked)

##Crock pot:##

Dump everything but the quinoa in the pot and go. Cook the quinoa about 15 minutes before serving.  Spoon out the quinoa and cover with chili to serve.

##On the spot:##

Chop the onion, green pepper, red pepper, garlic, serrano pepper, jalapeno and celery. Saute them with a little bit of olive oil until soft. Drain the beans and add in with the tomato and spices. Add the water and cook on medium for thirty minutes. Cook up the quinoa for your base and serve.
