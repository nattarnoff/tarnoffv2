---
layout: post
title: "Do\'s and Don\'ts for Web Accessibility"
date: 2009-04-17 11:38
comments: false
categories: [Accessibility]
---

While at this time the federal government has not made it mandatory or criminal if you do not, the larger your company is the more likely it could face a civil trial. Target Corporation has been through a suit filed by the American Council of the Blind in which the ACB won $6 million dollars and forced Target to accommodate the blind and visually impaired on their website. The ACB isn’t suing everyone, but they are pushing to make Section 508 a civil rights law rather than a government regulation.

<!-- more -->

Section 508 subsection 1194.22 of the ADA is the portion of the law that addresses accessibility for things from computers to software to the internet. According to the law the only people required to implement Section 508 are those providing services or product to or on behalf of the government (federal, state and local). If your company provides a product or service to a private company that is contracting with the government and that product or service will be the contracted interface representing the government, then you are obligated to adhere by Section 508 as well.

If you are not in one of these situations, but have a consumer facing web site that **MIGHT** be used by someone with a disability you should be following the law.

So without further ado…

##Do…##

- Provide at least one font size larger than your default. Users can increase size with their browser, but if you provide a link you can do it without breaking design which increases usability.
- Provide a high contrast mode. If that majority of your text and links aren’t black or dark blue text on white, give the user this option. You can do white text on black, but limit this to menus or highlight areas as it is harder to read long text.
- Provide alternate, title, or long description text for all non-text elements with a message. This includes images conveying a message (not spacer or decoration), multimedia, and form fields. If the alternate text (alt) is longer than 50 characters, use the long description (longdesc).
- Use tables for displaying tabular data only. This means if you think the presentation is best in a spreadsheet, use a table.
- Provide a summary for all tables using the “summary” attribute.
- Better than alternate text, use labels on all form elements. Do this by adding the “for” attribute to the label with the value equaling the id of the field it ties to. If the label doesn’t work, put the title on the field.
- Provide a link at the top to skip repeat navigation for the keyboard navigators.
- Provide a link at the bottom to take the user to the top.
- Instead of using a drop down for multiple select items, use check boxes (have you tried multi-selecting a drop down using a keyboard to navigate?)
- Any emphasis made using color, should also be understood without color. Bold it, underline it, box it, but just add something other than color alone.
- Provide accessible text versions of multimedia presentations that sync with the presentation.
- Make your pages so that they make sense when no style sheet is applied. This means navigation at the top, followed by content. Use headers to separate your content like a outline.
- Provide redundant links if you use a server side image map. Better yet, use a client side image map or none at all.
- Provide a header row and header column to all tables. Make sure that all the data cells and header cells relate appropriately if there are two or more logical levels of headers.
- Title all frames so that they can be identified and navigated easily.
- Make sure that if your page flickers, that it is between 2Hz and 55Hz.
- Make sure that any items relying on scripting to display can also be accessed when scripting is turned off.
- Provide a method to reset or extend time limits if a timed response is required.
- Provide a link to any plug-ins or applets needed to view the content you provide. These should also be hosted on compliant pages and be compliant themselves.
- Provide a text only version of the site if no other accessible alternate is available.
- Test your project in prototype form before implementing it.
- Use sans-serif fonts for long sections of text. These read better than serif fonts like Times New Roman when on a screen.

##Don’t…##

- Know if something is accessible? Try using it with a screen reader like JAWS and you will know. Close your eyes and forget the mouse to see how you do.
- Put blue or red text on black or black on blue or red.
- Make a Flash only site. Provide a text based version.

##Resources##

- Color blind check [http://www.vischeck.com/vischeck/](http://www.vischeck.com/vischeck/)
- Guidelines [http://www.w3.org/TR/WAI-WEBCONTENT/](http://www.w3.org/TR/WAI-WEBCONTENT/)
- Access Checklist [http://www.access-star.org/ITI-VPAT-v1.3.html#webdetails](http://www.access-star.org/ITI-VPAT-v1.3.html#webdetails)
- Color blind add-on for Firefox [https://addons.mozilla.org/en-US/firefox/addon/5001](https://addons.mozilla.org/en-US/firefox/addon/5001)
- JAWS screen reader [http://www.freedomscientific.com/products/fs/jaws-product-page.asp](http://www.freedomscientific.com/products/fs/jaws-product-page.asp)
