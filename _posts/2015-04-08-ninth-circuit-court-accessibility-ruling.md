---
layout: post
categories: ['Accessibility']
comments: false
date: 2015-04-08 11:45
title: "9th Circuit Court Accessibility Ruling"
---

The news just broke that the 9th Circuit court has ruled on two cases with major
consequences to the internet, how it is built, and how people use it. The
rulings are a huge mistake in interpretation of the law and should be appealled
immediately. However, work is currently being done to improve the Americans with
Disabilities Act that would in fact make these rulings moot. The question is,
which will happen first? Whether it is an updated ADA or appeals, it is still
not soon enough for people with disabilities.

<!-- more -->

!["A child in a wheelchair excluded from playground activities"](/img/disabled-chair-juan-monino.jpg)

## "Place of public accomodation"

The rulings were on *Earll v. eBay* and *Cullen v. Netflix*. In both cases the
Ninth Circuit decided that internet based business like the defendants are **"not
connected to any physical place"** according to Title III of the ADA†. This means
that since neither Netflix nor eBay have a physical storefront for purchasing or
consuming their goods, they are not required to comply with making their
websites accessible. 

In both cases the plaintiffs are coming to court from the perspective of hearing
impaired users. In *Earll v. eBay* the plaintiff wanted to be an eBay seller,
but eBay's verification process required an audio based step the plaintiff could
not complete. In *Cullen v. Netflix* the plaintiff was requesting closed
captioning be provided for all Netflix videos. 

To me, these are open and shut, but I guess I'm not inline with what the court
feels.

## Why this is wrong

**The ADA was first created before there was the internet we know.** It was created
when shopping from home meant opening up the Sears catalog and calling a phone
number. Under the ADA, Sears needed to provide technology for their
customers who were hearing impaired to also place orders. **It is also good
business practice to accomodate disabled users.**

Let's ask the question, what are eBay, Amazon, Zappos and their ilk? Are these
not in fact the internet's Sears catalogs? The difference between the two methods
of shopping from home are the way orders are placed. Instead of calling into a
call center and talking to a person, we fill our "carts" as if we were walking
through a physical store and use the self-checkout system. While there is no
physical store to walk into to purchase these products, it remains the same base
functionality. Sears needed to accomodate people in it's call center. Grocery
store self-checkouts need to accomodate disabled users as well.

The ADA came about before the idea of consuming our media online. In fact, it
wasn't possible to listen to radio or view television over the internet. Now
with people cutting cable cords or streaming music instead of buying albums, we
need to make sure we accomodate all users. 

Under the ADA, **television stations are required to have closed captioning** on all
their shows regardless if they are over the air or cable. When Dish and
satellite services for television came about, the standard held. It was still
TV and there is no "place of public accomodation" for TV stations. Yes, they
have buildings and offices in which they broadcast from. So does Netflix.
Netflix has replaced our video rental stores which had to accomodate people.
Netflix, and all streaming video services, are a combination of new TV channels
and video rental services. 

<p class="emphasis">Rules that apply to their predecessors under the ADA <em>must</em> 
apply to the internet age counterparts.</p>

I will admit if you read the words of the ADA, the Ninth Circuit has ruled on
what is said. However, courts have long held precedence that the words
themselves are open to interpretation in order to reflect the intent of the
authors. If we look at the **intent of the words** that were written before the internet, then we can see that the words themselves **do apply to internet based businesses** and the ruling is in fact incorrect.

## Legalized discrimination

What the Ninth Circuit has done by issuing these rulings is defined a method of
legalized discrimination. **They have declared that the disabled users are in
fact second class citizens on the internet.** They may recognize that they are
*real people* when our disabilities are in their face and they can't ignore us
without being rude. But out of sight, out of mind; **if they can't see us, they can
feel unashamed to treat us differently as if we don't really matter** (Note:
this applies to racism, sexism, ageism as well). 

Roughly 20% of the population qualifies as being disabled (not neccessarily for
government disability aid). **Nearly 100% of people have a disability**. Do you wear
glasses? You have a disability. Do you have to turn the volume up on the TV to
hear it? You have a disability. Do you struggle to read? You have a disability.
The list of things that remove us from the societally accepted *normal* physical condition and puts into the list of people who have a disability is long and virtually endless. This doesn't mean we are all disabled. Disabled is determined by how it affects our
quality of life and well being. 

**The reality is we are all perfect in how we are, there is no *normal*, and if
we don't enable and protect those who need assistance we are not a civil
society.** Personally, I'd like to live in a civil society.

&lt;/rant&gt;

### Citations
† [JD
Supra](http://www.jdsupra.com/legalnews/ninth-circuit-holds-that-ada-is-applicab-26474/)

Image: [Juan Monino](http://www.istockphoto.com/photo/left-behind-15896064?st=241bc38)
