---
layout: post
title: "Secret Project Taking Shape"
date: 2011-09-07 22:57
comments: false
categories: [Web Development]
---
This evening I finally got the code working on my new secret project. You can take a gander over at [http://www.verbosehaiku.com/new-haiku/](http://www.verbosehaiku.com/new-haiku/). This is a little haiku generator that will eventually become a service. Right now there is a small API in that if you add ?x= and a number, your haiku will go from 5/7/5 to be multiplied by the number you send (example: sending 2 gets you 10/14/10).
