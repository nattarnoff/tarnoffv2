---
layout: post
title: "Every Good Cook Needs a Few Good Tools"
date: 2009-07-03 09:11
comments: false
categories: [Cooking]
---
To do good cooking you need the right tools. The tools don’t need to be expensive, but they have to work. They need to be an extension of yourself, comfortable and reliable. They should also make your job cooking easier to maximize your time with family.Here is my list of tools that I use to make my dishes, and a good place for you to start.

<!-- more -->

##Breadmaker##

When I decided to go from vegetarian to vegan, the most difficult part of shopping became hunting down those hidden dairy and honey items. I couldn’t believe just how much of it was used throughout the foods I was eating. For me the worst culprit was bread. Other than some of the high end breads, just about anything “whole wheat” had honey in it. Well instead of paying $4 for a loaf of bread I decided to make them myself.
Baking bread can take quite a long time unless you have the right tools for the job, in my case, I went out an bought a bread maker. Just like a crock pot, breadmakers have a dump it in and set the time quality to them. You get great tasting bread when you want it with only the ingredients you want in it as well.
You don’t need to get anything too fancy, my mother has been using the same breadmaker for 17 years. I bought an Oster model that makes 1.5 and 2lb loaves for $55.

##Crock Pot##

This is a classic that can be picked up for as little as $15 from just about any department store. I recommend getting at least a 4 quart model especially if you are using any of my recipes as they are all based upon my 4 quart crock pot. When shopping for your crock pot, save yourself some labor in washing, by getting one that has pot that can be removed romt he heating element.

##Blender##

A quality blender can be used for many things. Everything from smoothies (green and link based) to salsa. I use mine daily, so be sure to get one with a good warranty. If you happen to be a gadget lover, go ahead and spend the money on a Vitamix or Blendtec model that can destroy an iPod (imagine what it could do to your leafy greens?), but I spent $35 on a Black and Decker with a 5 year warranty and it has yet to fail even when making green smoothies with kale and spinach.

##Food Processor##

The number one use I have for this item is when making hummus. I purchased a small 2 cup processor for this exact purpose. It cost me $15 and I use it two -three times a week. If you have a big family or know you would be making things in bigger volume, go ahead and get a large model. Keep in mind that a blend and food processor cross lines once in a while. Your food processor will be better for more solid foods, while the blender will be more for liquid based items.

##Knives##

This is probably the most important thing you can add to your tool chest. Buy yourself a nice set of blades. Since we are looking at vegan meals, you can probably skip the cleaver, but make sure to get at least the following knives:

- 7 inch Santoku – This is my primary knife use din 90% of my cooking
- 5 inch Santoku – I will use this when chopping small quantities of smaller veggies
- Bread knife for all the bread we will be baking
- 5 inch serrated knife – used primarily in cutting tomatoes and other soft veggies
- Paring knife
- Kitchen Scissors – Great for gutting stems and opening packages
- Steel – This is in place of a sharpener. A good set of knives don’t need a whetstone, but rather a steel rod of the same material and hardness to sharpen them.

I would look for a kit/block set that includes these items. If it is missing one or two items form the list you can always supplement, but try to get the same make and series as the rest of the kit so that you don’t damage them with the steel. Find knives with a full tang. This means the metal the blade is made out extends all the way through the handle as one piece. The handle will be bolted on to the tang. If you get the opportunity to open a box, heft the larger blades the way you use them when chopping. Make sure they feel comfortable in your hand. Always handwash your knives. You don’t want to risk corroding them with dishwashing detergent or letting them rust while they air dry (you do have your dishwasher set to air dry and not heat dry to conserve energy don’t you?).
