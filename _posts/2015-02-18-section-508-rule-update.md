---
layout: post
title: "Section 508 Rule Updates"
comments: false
date: 2015-02-18 11:45
categories: [Accessibility]
---

The United States Access Board has just released
[details](http://www.access-board.gov/guidelines-and-standards/communications-and-it/about-the-ict-refresh) on how they plan on
updating Section 508 of the Rehabilitation Act of 1973 as well as guidelines for
Section 255 of the Communications Act of 1934. These updates will have major
impacts to people providing technology and telecommunication solutions to
government entities and their subcontractors. They are open to public comment
for the next 90 days.

<!-- more -->

Be sure to go over and read the updates for yourself, but here is a high level
summary of what is changing: 

- Adoption of W3C&rsquo;s [WCAG 2.0](http://www.w3.org/TR/WCAG20/) standards with goals of meeting AA success
  criteria (applies to web as well as offline documents & software)
- Real-time text functionality to supplement voice communication (letter by
  letter trasnmission - could impact things like Google Hangouts & Skype)

The goal is to update the rules to accomodate changes since the rules were last
updated and how fast the technology is changing. 

This is a great set of changes being proposed as it will align the U.S.
government needs with that of the European Commission, Australia, Canada, New
Zealand and Japan. Additionally, they will be following an established set of
standards and not hodge podge of rules that have hampered the industry.

## This is not the ADA

Unfortunately we are still waiting for the American with Disabilities Act to be
updated. These proposed changes only really affect government agencies and the
tools they create and use. However, keep in mind that the federal court systems
have been ruling in favor of plaintiffs on lack of accessible features on
websites recently. H&amp;R Block, Netflix, Target, Harvard &amp; M.I.T. have all
faced recent lawsuits. With Section 508 finally getting updated, I think this
will only increase. 

The best advice I can provide is that you should start refactoring your site to
make [WCAG 2.0 AA](http://www.w3.org/TR/WCAG20/) compliant now.
