---
layout: post
title: "Jambalaya with Tofurkey Sausage"
date: 2013-04-07 13:28
comments: false
categories: [Cooking]
---
So this week, I had a craving for some good jambalaya, but I couldn’t find any recipe. I checked out a couple of non-vegan versions to see what kind of ingredients would be used. After reviewing a few things out there, I concocted this, which knocked my socks off. So I am here to share it with you, with two ways to cook it – On the spot, and in the crock pot.

<!-- more -->

## Ingredients

- 1 medium yellow onion
- 1 carrot
- 2 ribs of celery
- 1 green pepper
- 1 package of Tofurkey Italian sausage ( sausages) – Feel free to use your favorite vegan sausage
- 1 can (15oz) or two cups red kidney beans
- 1 can (28oz) diced tomatoes or 4 large tomatoes diced
- 1/4 tsp salt
- 1/4 tsp pepper
- 1tsp paprika
- 1tsp cayenne powder
- 3 cloves garlic, minced
- 4 cups vegetable broth
- 2 cups brown rice

First, if you are doing this on the spot, realize you will need 45 minutes to cook the rice as this is done separate. If you are doing the crock pot, start the rice 45 minutes before serving. To make the rice, use 4 cups water, all the rice and bring to a boil, then simmer 45 minutes.

##On the spot:##

Take your holy trinity (onion, carrot and celery) and saute with 1tsp olive oil and the garlic until soft. Add in the green pepper, sausage, broth, paprika, cayenne, salt and pepper to the other veggies. Add in the tomatoes. Before adding in the beans, make sure to drain and rinse them. Let this cook on medium heat for 30-45 minutes or until the sausage is cooked and the sauce is thick. If the sauces is running too much, add some of the partially cooked in rice to absorb the water. Plate some of the rice and add the jambalaya on top and you are good to go.

##In the crock pot:##

Dump it all in the crock pot but the rice. Set to high and let it run for 3-4 hours. If you can, check and stir it periodically. Serve over the cooked rice and enjoy.
