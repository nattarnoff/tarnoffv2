---
layout: post
title: "Pesto with Spaghetti Squash"
date: 2013-04-07 13:30
comments: false
categories: [Cooking]
---
Start out with making some vegan Pesto like I used in my [Spinach Linguini with Pesto and Avocado](blog/2013/04/07/spinach-linguini-with-pesto-and-avocado). Set this to the side and steam up your spaghetti squash. One the size of a standard American football should take an hour at 350 degrees. Don’t forget to poke holes with a knife to let the steam out.

<!-- more -->

Upon removing the squash from the oven, cut it in half carefully (it is hot) and scrape out the contents into a bowl, add the pesto, mix and serve. It works great with some sliced cucumbers or tomatoes on the side.
