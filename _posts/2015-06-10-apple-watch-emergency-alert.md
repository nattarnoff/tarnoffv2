---
layout: post
date: 2015-06-11 10:00
section: blog
title: "Apple Watch Emergency Alerts?"
---

I live with a person who has a very severe allergy to fish and shellfish. So much
so that going out to dinner is extremely hard. She carries an epipen in case she
is exposed, and it can be as little exposure as walking into a restaurant having
a fish fry. I have [malignant hyperthermia](http://www.nlm.nih.gov/medlineplus/ency/article/001315.htm), 
an extreme reaction to
anesthetics. It's genetic and my kids have it from both parents. My father has a
pacemaker. For us, one of the coolest things
to happen in iOS 8 was the ability to add the "Emergency" information, including
links to call an emergency contact to the lock screen of the iPhone.

That got [Sara](http://www.sarablackthorne.com) and I thinking, what could we 
do with an Apple Watch?

<!-- more -->

First, let's make sure we can get that information on the watch face. Especially
the medications, allergies, and pacemakers. Much like a medical alert
bracelet. Something quickly accessible to emergency medical technicians, first
responders, nurses, and doctors in case the wearer isn't able to communicate
their needs.

But we think the watch can go further. It has a heart rate monitor. Is there a
way to determine if someone is having a heart attack, epileptic siezure, or
going into anaphalactic shock by reading their pulse? What about a diabetic
crash before the user passes out? What if the watch was able to alert the user
to a problem before they knew it? What if your pacemaker can call 911 when it
can't handle your arrhythmia? 

First Alert has the "I've fallen and I can't get up" buttons you can wear around
your neck. What if the Apple Watch was that device? You could tap on it to call
for help. Maybe, because it has accelerometers built in, it can call
  automatically for you? Do we even know the limits of what we can do?

I know some of these features require having a phone. But not everyone has an
iPhone to make it work. It would be great if we could make it compatible with
Android and Windows Phone, but I'm also realistic that this may not be possible.

I haven't seen these things added to the Apple Watch yet, but they could be
there. If they aren't, why cant they be built? I think this is a great device to
help people who have health problems take care of themselves better (myself
included). I also think this is something that should be open source so the
community can build on it. I'm not an iOS dev, so I need some people to help
take on the coding of this. I'm happy to work on the product and user experience
side. If you are interested in this concept, [email me](/connect/) and lets see
what we can do.

**Full credit**: Sara came up with the idea and we brainstormed via audio iMessage while she was driving to Ohio.

**I'm calling out a few folks I hope will be interested in making this happen:**

- [Eric Knapp](http://twitter.com/ejknapp)
- [Alex Harms](http://twitter.com/onealexharms)
- [Aaron Douglas](http://twitter.com/astralbodies)
- [Janie Clayton](http://twitter.com/redqueencoder)
- [Georgi](http://twitter.com/georgicodes)
- [Strand McCutchen](http://twitter.com/strabd)
- [Kerri Miller](http://twitter.com/kerrizor)
- [Jim "Big Tiger" Remsik](http://twitter.com/jremsikjr)
- [Jaimee Newberry](http://twitter.com/jaimeejaimee)
