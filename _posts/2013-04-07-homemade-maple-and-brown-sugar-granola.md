---
layout: post
title: "Homemade Maple and Brown Sugar Granola"
date: 2013-04-07 13:23
comments: false
categories: [Cooking]
---
One of the best things I did when I became a vegetarian is transition my family off of cookies and sweet, sugary cereals and onto to granola as our treat. We bought granola bars, granola cereal and I think I even tried a granola bread or two. Life was dandy.

<!-- more -->

Then I went vegan, and as a vegan I read everything. I was astounded to find out how often honey made it into anything granola. Worse yet, were the the ones that had ingredients like casein. Why was casein in a granola bar? So I started playing and this was my first granola hit.

##Ingredients:##

- 2 cups quick oats
- 1 cup walnuts lightly crushed
- 1 cup sunflower seeds
- 1 cup puffed rice cereal
- 1 cup raisins
- 1/4 cup maple syrup
- 1/3 cup brown sugar
- 2 Tbsp margarine
- 1/4 tsp sea salt

##Cooking:##

Preheat the oven to 350 degrees. Spread the nuts, oats, seeds, and puffed rice out on a cookie sheet in as thin a layer as possible. Once preheated, place the cookie sheet in the oven for about 15 minute to toast the dry goods. When the oats have a nice golden color to them, pull the tray out and dump them into a big bowl.

Over medium heat dissolve the sugar, salt and margarine into the syrup. Once you have a nice smooth and liquid concoction pour over the dry goods and add the raisins. Mix the combination until everything is wet.

##Granola Bars:##

If you are trying to make granola bars, pour the mixture into a 13×9 pan lined with parchment or wax paper. Make certain to push it into all the corners and as even a thickness as possible. Cover with more parchment paper and then with as many heavy books or pots as possible. Let this sit for a couple hours to cool and solidify.  Once it is cool, cut them into bars and wrap in cellophane to keep them fresh.

##Granola Cereal:##

Spread the mixture out onto a parchment covered cookie tray to dry and cool. Once dry, break up any large chunks to bite sized portions. Put into an air tight container and you are ready to go. This makes a great topping for vanilla rice cream or soy ice cream.
