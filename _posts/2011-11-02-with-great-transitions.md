---
layout: post
title: "With great transitions"
date: 2011-11-02 13:56
comments: false
categories: [Accessibility]
---
Websites that have lots of movement have always bugged me a bit, but lately they are bugging me a lot more. I don’t know if this is because something in me is different or because or because I have become sympathetic to those that have disorders where motion affects them (Like [Marissa](http://tarnoff.info/2011/09/vestibular-disorders-and-the-internet/)).

<!-- more -->

Lately there has been a lot of talk on the web about the awesomely cool things that we as designers and developers can do with [CSS3](http://tympanus.net/Tutorials/OriginalHoverEffects/index.html), namely transitions, easing, animation and rotations. But just because we can do these things, should we? Isn’t it our responsibility to make a eb that everyone can use? I find myself recoiling as these things advance because not only are they difficult for some people to use, but I find them irritating as well. The more we can do, the simpler I want my sites to be.
