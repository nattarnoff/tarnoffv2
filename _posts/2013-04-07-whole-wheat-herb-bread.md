---
layout: post
title: "Whole Wheat Herb Bread"
date: 2013-04-07 13:26
comments: false
categories: [Cooking]
---
This is my favorite bread. It rocks with any soup, chili or jambalaya dish that you put together. It also has the freedom to change up the spices and herbs to match what you like or the dish you are concocting. I make this with my breadmaker, but you could also do it by hand. You would have to mix the ingredients, let it rise, kneed it and rise again before baking though.

<!-- more -->

##Ingredients:##

- 1 2/3 cup warm water
- 1/3 cup packed brown sugar
- 2 tsp salt (I always use organic sea salt)
- 2 Tbsp margarine (I prefer Smart Balance Vegan blend or Earth Balance Olive Oil spread)
- 4 2/3 cup whole wheat flour (try to go organic if you can)
- 3 tsp bread maker yeast
- 1 tsp extra virgin olive oil
- 1/4 tsp basil
- 1/4 tsp oregano
- 1/4 tsp marjoram
- 1/4 tsp dill weed

Suggested herb alternates: rosemary, thyme, caraway seed

In your bread maker add the ingredients except the oil and herbs in the given order, making sure your yeast doesn’t touch the wet ingredients or sugar. Set your machine to whole wheat and let it run. At the “raisin” beep (after the rising), mix the herbs in the oil and pour over the top of your dough. If you are setting this up to be done when you come home, add the oil and herbs before the flour.

I like a light crust, so I set the timer to be 10 minutes longer than when I want it done and pull it out early.
