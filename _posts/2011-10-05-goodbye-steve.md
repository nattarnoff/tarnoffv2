---
layout: post
title: "Goodbye Steve"
date: 2011-10-05 07:26
comments: false
categories: [Remembering]
---
I grew up on Apple computers. They encouraged me to drawn, animate, write prose and code. they were easy to use and were the thing that I always came back to. Now, I use the beautiful tools he has designed (well lead the design of) every day. He has given me a career that makes life for my family comfortable.

<!-- more -->

I never knew the man. I never visited the campus he worked at, but I feel a kinship to him and find myself choked up over the loss. I wish his family the best and I while he will be missed in geekdom and beyond, he finally doesn’t have to live with what I am sure was a painful disease. Rest in peace Steve Jobs.
