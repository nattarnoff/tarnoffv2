---
layout: post
title: "A Way to Express Oppression"
date: 2016-02-15 14:00
comments: false
categories: [Equality]
---

Social media is a tool that can be used for great good (rallying rebellion and
protests against tyranny), great evil (rallying mobs to harrass people), or
plain fun (cat gifs). Most of us using social media will surround ourselves with
people we want to communicate with. A lot of times, these people have the same
political, social, and economic views as us, but in some cases they don't. We
all have that one uncle or aunt that is our polar opposite, but because they're
family we try and tolerate their extreme ideas.

<!-- more -->

Facebook is particularly bad in this. We use it to connect primarily with family
(Whereas LinkedIn is professional and Twitter is...well, what is Twitter?).
Because it's family we get a lot of differing ideas, especially as older
generations get on to find out what their kids and grandkids are up to.

It was on Facebook where I encountered an honest question that demonstrated a
lack of understanding a large portion of this world seems to share. And I think 
I came up with a decent analogy to help explain it in a
way that seemed to get across to this individual, so I decided I'd share it more
publicly without shaming the person who asked.

## The Kindle

A friend posted to her Facebook wall a photo of two young black women holding a
poster that said, "Pro Black isn't Anti White." This is something I truly
believe in, but someone else didn't see it the same way and asked:

> "is pro white anti black?"

I responded with, "Yes. If racism didn't exist, you could be proud to be who you 
are without being anti anyone. However, since racism continues to be a reality, 
pro white carries with it nearly 500 years of oppressing minorities that can't 
be tossed aside. You can continue to be proud of who you are, but to promote 
white is continuing this oppression." 

I know that I am lucky. I am white. I am perceived as male and have live most of
my life as such. I make good money. And this is why I'm trying to keep my eyes
open to my privilege and give opportunity to those who don't have my privilege.
I try not to speak up when it is something I don't have direct experience with,
race for instance. But when there is no one else speaking, I can't stand quiet.
I just hope I say the right thing.

Apparently this didn't convince the individual, who responded with a standard
response that doesn't recognize his cis white male privilege. Again, I don't
want to shame, so I'm not including that text. It's what happened next that I
truly want to share, my response.

In my work as a user experience and disability advocate, I find situational
disabilities a good way build empathy for users. But there is no way to simulate
to a white person what it is to be Black or Latinx. So we need to demonstrate
how oppression works in a method everyone can get.

## The Response

"We're going to pretend to play blackjack. Except it's a little different. 
We have two decks. All the cards valued 9 and up are in one deck. The rest of 
the cards are in the other. As a cis white man, your hands will be dealt from 
the 9+ deck. LGBT, people of color, and women playing get deals from the other 
deck. Is it possible for them to win a hand against you? Yes. But how likely 
will it be? But this gets even worse. If you are Black or Latinx, you only get 
cards 4 or under. Women have the 8s removed. LGBT can have 6, 7, and 8 removed 
depending on if they are cis or trans. 

Let's say you were raised in the upper middle class, you get 2 extra kings 
from a third deck. Born in the 1%? You get the other two kings and the aces. 

Pro-white is maintaining this status quo which is clearly biased. Pro-Black, 
pro-woman, and pro-LGBT is taking the original two decks, shuffling them back together and 
dealing to everyone from that same set. Pro-Black is not as bad or close to 
the same as pro-white. 

I hope, you can understand this and help level the playing field."
