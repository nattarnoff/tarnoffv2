---
layout: post
date: 2016-03-07 14:48 
section: blog
categories: adventures
title: "Joining a New Team of Accessibility Professionals"
---

The last two and half months have been quite an adventure. During this time I
applied to work with over 50 companies. I conducted interviews with 28 of them. I went to multiple rounds of interviews with a dozen; and 8 went as far as doing whatever the
company used as a technical guage or code test. Discussions with four companies
got to the point of discussing salary. I visited in companies on both the east coast and the west coast.

<!-- more -->

Through it all, I was able to maintain a pretty positive outlook despite
thinking I would be back to work sooner than I actually am going to be.I had a
lot of help in finding leads, getting recommendations, and keeping my head on
straight through what turns out to be a great transition in my career. The
positions I was interviewing for varied greatly from design and user experience,
to front end code devellpment. Some were working on product, others working with
marketing. Some were startups, one was a Fortune 500, and others were local
agencies. It's all stuff I can do.

Over the last decade I've worn just about every hat there is in the web
development industry. I've built, secured, and deployed servers, databases, and
back end code. I've worked on mobile, insurance, ecommerce, government, and
marketing projects. I've written some cutting edge front end code and repaired
some highly inaccessible code. I've handled user testing, design, user
experience, and requirements gathering. But through all of it, I have always had
a proclivity to emphasize my work on accessibility. Which is why I'm happy to
announce that starting tomorrow morning...

## I'm Joining the Team at SSB Bart Group

As a Technical Consultant. [SSB Bart Group](http://www.ssbbartgroup.com/) have
created AMP, the Accessibility Management Platform, to help business identify,
track, and fix accessibility issues on their websites. Additionally, they
provide companies with accessibility training, auditing, and meeting their
compliance standards. 

In my role of a Technical Consultant, I'll be helping clients to define their
testing, conduct testing, provide reporting and technical consultation, and
mentor SSB Bart Group's analysts and consultants on technical issues. This
represents a major shift from being development and UX as a primary focus to
accessibility being 100% of my job over these areas. And I am super excited.
While I've had great roles over the last several years, this is what I've been
working towards.

## Thank You

I wouldn't have gotten here without my friends and collegues in the industry.
Everyone of you that reads this post, I've met in person or worked with, or
perhaps was just a friend all helped me find this role that is the perfect fit
for me. In particular, there are a few people I'd like to call out because they
  have gone above and beyond in helping my career: Jim Remsik, Peter Aitken, Ed
  Charbenau, Derek Featherstone, Elle Waters, Natalie Patrice Tucker, Aaron
  Douglas, Aria Stewart, Aki Braun, Marcy Sutton, Jen Strickland, Jason Hoover,
  Chris Eigner, and Dylan Barrell. Thank you everyone, and off to the adventure! 
