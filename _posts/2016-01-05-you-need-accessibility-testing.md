---
layout: post
date: 2016-01-05 14:48 
section: blog
categories: consulting
title: "You Need Accessibility Testing"
---

This probably isn't the first time you've heard it. Somewhere along the line
somebody asked you how well your site or application works for people that are
blind. You probably mumbled something along, "They don't use our stuff," or
maybe, "I don't know, does it matter?" The short answer is, yes it matters. And
you want to pay attention to why.

<!-- more -->

## Accessibility isn't Just the Blind

Accessible products and websites are often built with the blind in mind,
however what it means to be truly accessible runs much deeper than that.
Accessible by it's very nature means that anyone, anywhere, with any tool of
their choosing can use it. This means the disabled, which is not just the blind,
but those with hearing, neuro-diversity, mobility, physical, and
learning disabilities; and it means the abled that are compromised by other means. 

Economics plays a huge paert in accessibility. AOL still has 2 million people
using dialup internet. This is an economic issue. ISPs have decided it isn't
beneficial enough to run faster lines to these areas or they are on fixed
incomes and can't afford the rising cost of internet access. Developing nations
don't have access to the funds to buy the latest and greatest technology, so
they use products released years ago to get online. 

As developers we all cheer when we hear that Microsoft is dropping all support
for browsers below Internet Explorer 11 next week, but how does someone in an
  African country who can't afford to upgrade from XP going to feel when we
  don't support IE9? And what if that great new mobile application you created
  only works on iOS 9+ and Android 4.4.4? Many lower income folks in the
  developed and developing worlds have phones that are 2, 3, 4, or 5 years old
  and don't support the latest things we can do with the web or mobile.

*Building accessible products and sites means taking all this into account.* It is
using responsive web design to make sure your content is readable on all devices
regardless of size. It is using solid HTML to make sure the content is parseable
by a variety of technologies on the client side from browser, to apps, to
refridgerators and TVs. It is using progressive enhancement to make certain that
the product is usable even when the CSS or Javascript fail to load. It requires
building products that while that may be single page applications running on
Angular, React, or Ember continue to be useful applications when the CDN doesn't
connect or the connection is slow. It means setting a performance budget so that
we aren't trying to load 3 megabytes of data to run a blog. Knowing to reduce
images, minify scripts, use CDNs to load things faster, and trying your darndest
to limit any single page to something that will load in under 4 seconds and be
usable.&dagger;

## But Why?

Getting back to the disability side of things, recently the DOJ has takend to
pushing through that public websites are liable to be accessible to the
disabled. There is recent legislation that is forcing airlines to improve their
websites. Netflix, Target, H&R Block, and eBay have all been sued by disabled
people because their websites weren't accessible. 

You probably have a good idea what a lawsuit brought in federal
court would cost your company even before remediation and damages are incurred.
Why get sued when you can save money fixing the problems first? 

## I'm Here to Help

Your competitors haven't solved this problem yet. Many are in the same boat as
you and don't where to start. 

With over  decade of experience in front end development, user experience
design, and accessible development I can get you ahead of the curve. Working
together we can do a proper assessment of your website in all the major
assistive technologies to make certain any deficits are found before you end up in court. With 
an active assessment done and a remediation plan in place you can resolve many
legal actions before they get started. 

Once you know where the problems lay, I can work with or train your
team on how to resolve those issues bringing your site up to par with WCAG 20.
AA standards. This is the standard level used by the Canadian, United Kingdom,
Austrailian, and Japanese governments to write their web accessibility
legislature. The European Union has used it to craft theirs; and the United
States has similar legislation based on WCAG 2.0 AA facing it for a vote. 

[Contact me](/connect) to set up a time to discuss your needs so we can set your
product or site off as a differentiator in your industry because it is
accessible.

&dagger;  4 seconds is a magic number that will cause people to think your app is
slow. If your customer is using dialup, then a 500kb page will take 1 minute and 13 seconds to
download. Sure 3 megabytes is instantaneous on your home cable connection or 4G
smartphone, but in the developing world it could be a minute.  
