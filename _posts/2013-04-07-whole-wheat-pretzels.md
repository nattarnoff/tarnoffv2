---
layout: post
title: "Whole Wheat Pretzels"
date: 2013-04-07 13:25
comments: false
categories: [Cooking]
---
When I was a kid growing up on the east coast we would travel to New York City and Philadelphia. Each time I would beg my parents to get one of the soft pretzels from the guys on the corner. Nothing was better as a kid, and they remain one of my favorite snacks to this day. They fill you up and aren’t loaded with all of those horrible chemicals. The calorie count is low, and with my recipe using dark rye and whole wheat flours you get a better fiber and protein count into your system.

<!-- more -->

##Ingredients##

- 3 cups Whole Wheat flour
- 2/3 cup Dark Rye flour (you can sub Rye flour if you desire a lighter pretzel)
- 2 tsp light brown sugar
- 1 tsp salt
- 1 1/4 tsp yeast
- 2 cups water

Mix the flour up in a large bowl with the salt, sugar and yeast. Once it looks like the yeast, salt and sugar are evenly dispersed, slowly mix in one cup of water. At this point I found the best way to mix is with my hands. The dough is really clumpy at first and we will never be adding enough water to make it into a liquid, so hand needing worked best. Once the first cup of water is worked in, slowly add more water until you have one solid piece of dough. It should look a little wet and feel a little sticky at this point.

On a clean surface, sprinkle a little all purpose flour (about 1 tbsp) and spread it around. Need the dough for about 5 minutes, making sure to add a little flour if it becomes too sticky to work with. It should work itself into a nice even ball. Place in a bowl and cover the bowl with cling wrap. I like to spray a little canola oil on the cling wrap in case the dough rises enough to touch it. Let the dough rise for 1 hour in a warm place.

After the dough has doubled in size, we are going to form our pretzel shapes. Get a cookie sheet and spray it with your canola oil. Break up your dough. The number of pieces depends on how big you want the pretzels. You can make 8 pretzels in the Bavarian size (about 4 inches across), 4 large street corner pretzels, or 16 snack sized. Roll each piece of dough out into an even snake. If doing 4 or 8 pretzels, the dough will be about 1/2 inch thick. A little thinner for 16. Next, loop the dough twisting the ends together ad folding them back into the dough for your shape. Have fun here, you don’t nee to be stodgy on design especially if working with kids.

Once all your pretzels are formed, cover them again for 30 minutes with oiled cling wrap.

Preheat the oven to 400 degrees and bake for 10 minutes. While this is going on, you need to decide if you are going to coat them with salt or eat them low sodium. If salting, take 3 Tbsp water, 3 tsp salt and dissolve them over medium heat. When the pretzels are pulled from the oven, shut the oven off, paint this mixture over your pretzels and sprinkle on some course seas salt to taste. I like to put the pretzels back into the oven for 5 minutes with the heat off to dry the salt in place. After that, let them cool on a wire rack.
