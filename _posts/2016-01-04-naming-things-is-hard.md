---
layout: post
date: 2016-01-04 14:48 
section: blog
categories: gender
title: "Naming Things is Hard"
---

On October 13th, I posted on here about the fact that I'm genderfluid & non-binary. At the
time that was all I was willing to post. However, over the last couple months I
have realized that my birthname isn't one I'm comfortable with. So I picked a
new one.

<!-- more -->

For the moment, I'm still legally Greg. And probably will be for a few months.
Maybe longer. But for casual use, I would like to be called Nat.

Some folks have asked me about the name. I picked it because of my favorite name
ever, Natalie. A friend in school named Natalie was often called Nat for short.
But I also knew a Nathan who occasionally used Nat. It struck me as a name that
could be feminine or masculine or neither, which really describes me on a given
day. 

Additionally, I'd like to use the pronouns they/them/their in the singular
fashion instead of he/him/his. I know this is a little odd for some folks. The
english language for the last century has said 'they' is a plural pronoun.
However, going back in the English language we can see it being used as a gender
neutral singular by the likes of Shakespeare. Additionally, many people don't
realize it, but they use it as a singular or number indeterminite all the time.
For example, you are driving down the road and someone cuts you off. You can see
there is only one person in the vehicle, but you can't tell anything more, so
you shout, "They're an idiot!" Welcome to singular they. Lastly, many of the
worlds newspapers are adopting singular they pronouns at this pioint.

In the near future I will be switching my social media and online profiles to
Nat as follows:

- Twitter - @inknpierce (already using this. Will merge accounts later some magical way)
- Github - nattarnoff from gtarnoff
- Bitbucket - nattarnoff from gregtarnoff
- Facebook & LinkedIn when it is legal
- Instagram - already inknpierce

Anything I've forgotten will become nattarnoff as well.

## Update

The following social media accounts have been updated to my new username,
@nattarnoff except Instagram which is @inknpierce.

- Twitter - [@nattarnoff](https://twitter.com/nattarnoff)
- Github - [nattarnoff](https://github.com/nattarnoff)
- Bitbucket - [nattarnoff](https://bitbucket.org/nattarnoff/)
- Instagram - [@inknpierce](https://www.instagram.com/inknpierce/)

