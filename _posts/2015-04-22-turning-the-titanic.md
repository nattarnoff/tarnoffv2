---
layout: post
title: "Turning the Titanic"
comments: false
date: 2015-04-21 12:01
categories: [Mobile]
---
In 2013, I was working for a Fortune 500 financial firm. During my four years there I had
been pushing to make mobile development a first level priority. At the time it was a
forgotten step-child. I had the opportunity to present on my experience at
[CodePaLOUsa](https://codepalousa.com) that year. Given Google's new policy on
mobile and the fact that there are still big firms that aren't mobile friendly,
I thought I'd resurface the tale.

<!-- more -->

##Getting Large Enterprise to Think Mobile First

!['Profile of the Titanic'](/img/titanic.jpg)

##This is Not a Success Story

When I joined the company they had just launched their first app (Blackberry
Storm with iOS 3 months away) and their first "mobile" website. The site was
being served up via a third-party proxy into what was only slightly more than an old school
WAP site. It was already 2009 and the iPhone had changed the landscape of the
web. Responsive Web Design hadn't taken hold quite yet, but it was clear we
could do rich mobile applications on the web.

With every new design, product, and campaign I pushed to take a mobile first
approach. Management pushed back. They didn't see the need; they thought people
wouldn't be using smartphones for researching their financial future. 

Meanwhile, our traffic was transforming. The main corporate site saw between
800,000 and 1,000,000 uniques a month. From March 2012 through March 2013,
mobile traffic grew from 72,000 uniques to nearly 150,000 uniques. More than
double in a year resulting in 16% of the monthly traffic. After that, it really
got interesting. We began seeing the uniques double monthly for mobile users. 

Despite this growth, it wasn’t until the company executives traveled to a vendor for an 
advertising meeting did they understand the impact of mobile. On a trip to Google, sitting down 
with Google's senior leadership, they were given a presentation showcasing mobile 
first and why it was needed. This presentation was around advertising on the 
Android platform, but our management bought into everything mobile and came back 
with a new purpose, but no idea how to do it. My efforts talking to lower level
management did nothing to change the direction of the mobile debate.

###The Lesson: Don't Start at the Bottom

Head straight to the C-suite. If you can get them to buy in, they will mandate
it down the chain. Find resources out there making mobile first their focus,
especially ones that are direct competitors. Here are a few I recommend:

- Burton Snowboards
- Google
- Microsoft
- Boston Globe
- BBC

##Implementing the Corporate Way

One of the problems we ran into is working in an environment that despite 
using "agile" was slow to approve, process and plan projects. Everything had 
to be documented, estimated and comped before presenting to the committee for 
approval. Once approved the hours needed to be slotted somewhere, often months away, 
and assets, I mean resources...er people, assigned to the project.

I worked on the Marketing team. This had some serious disadvantadges. I/S didn't trust
us. They even resented us doing some of the work, but couldn't provide talent to 
take over either. 

##Be Subversive

Being in Marketing also had some perks. Since we moved fast (sometimes our
deadlines were 3 weeks away), we could experiment and try new frameworks, tools,
and techniques.

This led to us making moves when we couldn’t get things approved or the 
estimates were ridiculous. When we couldn’t get approval to do things through 
the proper channels, we decided to retrofit in whatever manner we could. We
began building campaigns with frameworks like Bootstrap and Foundation to see
what worked. We used style tiles and mood boards to develop modules instead of
sites. We shifted our presentations to stakeholders to show more wireframes (in
browser if we could) and fewer Photoshop files. We stopped offering mobile as a
choice to each project. Our estimates included mobile development time.

##The Win

Successes with retrofitting and advertising campaigns made the C level realize 
we can and should be doing mobile first. We got the greenlight eventually to
move forward. All new projects were deemed "mobile first", but most were still
months down the road to be implemented (not too surprising, they haven't changed
the corporate website in 5 years). The corporate website still uses an m. site
and isn't responsive. 

##Summary

- Start at the top, don't bother with your immediate supervisors
- Present design in style boards & wireframes
- Design modules, not sites
- Use a CSS framework to speed up your production
- Don’t give your client a choice. They’ll thank you later.

