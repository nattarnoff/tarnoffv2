---
layout: post
date: 2015-09-04 10:00
section: blog
title: "Granting the User Control"
---

More than once I have spoken about giving the user control over their
experience. Too often we make assumptions about how things should work, but in
the end those assumptions always forget someone. Providing a set of tools for
the user to control the situation or experience allows us to have the most rish
and dynamic experience we can build, but one that any user can get behind even
if they can't do all the fancy bells and whistles.

<!-- more -->

One such example is animation in social media. We love (I know I do) to post
animated gifs, videos, vines, and cool new web animations in a place where our
friends and collegues can see. For me, many of these (especially videos and
animations) will trigger a vertigo atack. The wrong kind of animation could
cause someone with photo-sensitive epilepsy to have a seizure. I have a little
faith left in humanity and hope no one posts these kinds of things with the
intent to harm, but we can do better.

I'm not asking you to stop posting these. Instead I'm asking to give your
followers control. If you come across a video full of violence, profanity, or
sexual content while at work (or somewhere you would be embarassed to get caught
viewing it), you appreciate when that video has a NSFW (not safe for work) tag
on it. We have learned what this means and now have the choice to view it or
save it for when we are less likely to be embarrased if caught.

We also have TW (trigger warning) for articles that contain descriptions of
sexual assault, abuse, or suicide. We recognize that some people out there will
be dramatically affected if they read these things, so we let them know up front
it could happen. They can then choose to keep reading or not. They have the
choice and the control. 

So, I'm pushing for two new tags. One warns users that the content may cause
dizziness, the other warns against possible seizure inducement.

## #DZY

DZY, short for dizzy, but a clear concise item that takes up no more than 4
characters (if including the octothorpe - #). It communicates that the following
content may cause some vertigo, motion sickness, or dizziness in some users.
Putting this in a tweet with a link allows users like myself to decide if they
want to click on it. The user has control. They can click through knowing they
will see movement (knowing goes a big way in prevention), or if they are having
a bad vertigo day (like I am right now) they can avoid it completely.

## #SZR
SZR, short for seizure, is the same concept but for things that strobe and
flash. Nothing should ever be created in a strobing or flashing manner that
riggers epileptic attacks, but if you aren't sure if it will, you can now warn
your followers and users that something is coming that could.

Like all new things, we need to get movement on these in order for them to
stick, so please go preach. You can retweet my orgiginal #DZY tweet if you want: 

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/a11y?src=hash">#a11y</a> Please use <a href="https://twitter.com/hashtag/DZY?src=hash">#DZY</a> for trigger warnings on links with animation to warn those of us with motion sickness issues.</p>&mdash; Awkward Dad™ (@gregtarnoff) <a href="https://twitter.com/gregtarnoff/status/639568343326834688">September 3, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Or the one I just posted that includes both tags:

<blockquote class="twitter-tweet" lang="en"><p lang="en" dir="ltr">Help me get these moving: Use <a href="https://twitter.com/hashtag/DZY?src=hash">#DZY</a> to warn people of possible dizziness inducing links and <a href="https://twitter.com/hashtag/SZR?src=hash">#SZR</a> for possible seizure inducing links. <a href="https://twitter.com/hashtag/a11y?src=hash">#a11y</a></p>&mdash; Awkward Dad™ (@gregtarnoff) <a href="https://twitter.com/gregtarnoff/status/639793132289130496">September 4, 2015</a></blockquote>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

Thanks for helping spread the word!
