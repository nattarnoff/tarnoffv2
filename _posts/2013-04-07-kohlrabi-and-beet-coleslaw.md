---
layout: post
title: "Kohlrabi &amp; Beet Coleslaw"
date: 2013-04-07 13:24
comments: false
categories: [Cooking]
---
So this past week in the CSA we received a head of cabbage, my first kohlrabi ever and some beets. I don't remember the last time I ate a beet. I have never been a big cabbage fan. But seeing these three things in the box, all I could think about was...coleslaw! The recipe was "baked" in my head before I knew it, I just needed to find a good slaw sauce that was vegan because most are made with mayo or at least egg.

<!-- more -->

Digging around I didn't really find any, but I did come across a few DIY slaw sauces using mayo, so I subbed in some vegan mayo from the store (the only ingredient I didn't already have (we don't really do mayo in my house). Since I split my CSA share, the recipe is based on this, but believe me, this is a lot of slaw!

##Slaw Sauce:##

- 1/3 cup vegan mayo
- 3 Tbsp white wine vingar
- 2 Tbsp soy or rice milk
- 1 Tbsp sugar
- Salt and Pepper to taste

##Veggies:##

- 1/2 head of cabbage chopped/shredded to slaw size (do what you like here)
- 2 medium carrots shredded (we went superfine and this became mush, so go matchstick size)
- 1/2 kohlrabi : peel off the skin if woody and julienne to match stick size pieces
- 1 beet (yes, raw) : peel and julienne to match stick size pieces

Mix the veggies with the sauce and chill a bit before eating. You can eat at room temperature, but I like mine cold.
