---
layout: post
date: 2015-10-14 10:00
section: blog
title: "Hello World"
---

This isn't an easy post for me to write. I hadn't planned on writing it yet, but
then on October 2nd, 2015 I gave a talk and while I knew what is in my slides, I
didn't realize I was outing myself while I was speaking. I guess I was just in
the zone. This is what happens when you don't rehearse and know your lines
verbatim. You wing it and say what works in the moment.

<!-- more -->
>I am not doing this to make you comfortable

## I Am Not Heterosexual

<img class="left circle" src="/img/IMG_0907.jpg" alt="Self portrait" />

I'm not really sure how much more to define it. I'm still trying to figure out
the whole thing. What I can say is that I often have romantic feelings for more
than one person at a time. Additionally, my feelings are based on emotional
connection and not physical connection. I'm equally attracted to men, women,
non-binary, and transgender people. 

However, I'm not sexually attracted to anyone anymore. I used to be, but that
changed a while ago; I'm not sure when. I like to cuddle, hug, hold hands, and
with the right people kiss. I have no interest beyond this. Some people call
this "gray ace" as in asexual, but with some caveats. I don't know if this best
describes me, but it's close.


## I Am Not Cis-Male

I'm gender non-comforming. Non-binary. Gender-fluid. Gender-flux. Whatever you
want to call it. I exist in the gray area between the traditional definitions of
man and woman. I've never fit into the societal indoctrination I received of
what it means to be a "man". I'm emotional. I care about people a lot. I don't
like anger (although I have had a lot in the past). I don't like violence, cars,
woodworking, lawncare, football, etc. I've always been drawn to nurturing people,
children, friends, family, students. I've always desired to care for others. I
have pursued the arts. I enjoy cooking. These are things soceity has told us are
the roles of women. 

Well fuck that. **Gender isn't binary.**

## Gender isn't About Your Gentials

There is gender, sex, gender expression, and sexuality. Gender is how you feel.
Sex is what parts make up your body (but even this is not binary). Gender expression is how you present yourself. Sexuality is who you love. 

I have found a way to be comfortable with my constantly shifting gender. My
gender expression has me mixing traditional men's and women's clothing to a
unique style I feel comfortable in. The beard is staying for now, and this will
cause some cognitive dissonance for some people. If you have a problem with me
in a skirt or tights and having a beard, well...Fuck you. **I am not doing this to
make you comfortable.** I'm doing to be myself. I've already noticed an
improvement in my vestibular disorder, migraines, stress levels, anxiety, and
depression just by accepting who I am and no longer repressing it. I'm only writing this so I have something to point to because I'm
not going to take the time to explain it to you.

Here I am. You can take it or leave it. But I'm damn good at what I do, so I'm
going to keep doing it.
