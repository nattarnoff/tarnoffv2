---
layout: post 
category: projects
title: Flexbox Prefixes
link: https://github.com/gtarnoff/flexbox-prefixes 
---

I had an itch needing prefixes for the modern Safari browsers. This is a Sass
library to add those in.

<!-- more -->

###[Check out the project](https://github.com/gtarnoff/flexbox-prefixes)
