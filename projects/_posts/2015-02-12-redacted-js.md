---
layout: post 
category: projects
title: Redacted.js
link: http://gtarnoff.github.io/redactedjs/ 
---
JavaScript library and bookmarkelt to redact foul language from screens for demo
purposes in a CIA blackout marker style.

Redacted.js is a small library to simply blackout those words (or an array of words you pass) so they don't appear in your page.

<!-- more -->

### [Check out the project](http://gtarnoff.github.io/redactedjs/)
