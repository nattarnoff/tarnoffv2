# -*- encoding: utf-8 -*-
# stub: jekyll-assets 0.7.8 ruby lib

Gem::Specification.new do |s|
  s.name = "jekyll-assets"
  s.version = "0.7.8"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["Aleksey V Zapparov"]
  s.date = "2014-05-08"
  s.description = "  Jekyll plugin, that allows you to write javascript/css assets in\n  other languages such as CoffeeScript, Sass, Less and ERB, concatenate\n  them, respecting dependencies, minify and many more.\n"
  s.email = ["ixti@member.fsf.org"]
  s.homepage = "http://ixti.github.com/jekyll-assets"
  s.licenses = ["MIT"]
  s.require_paths = ["lib"]
  s.rubygems_version = "2.1.9"
  s.summary = "jekyll-assets-0.7.8"

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<jekyll>, ["< 3.0.0", ">= 1.0.0"])
      s.add_runtime_dependency(%q<sprockets>, ["~> 2.10"])
      s.add_runtime_dependency(%q<sass>, [">= 0"])
      s.add_development_dependency(%q<bundler>, ["~> 1.3"])
      s.add_development_dependency(%q<rake>, [">= 0"])
      s.add_development_dependency(%q<rspec>, [">= 0"])
      s.add_development_dependency(%q<compass>, [">= 0"])
      s.add_development_dependency(%q<bourbon>, [">= 0"])
      s.add_development_dependency(%q<neat>, [">= 0"])
      s.add_development_dependency(%q<bootstrap-sass>, [">= 0"])
      s.add_development_dependency(%q<sass>, ["~> 3.2.13"])
    else
      s.add_dependency(%q<jekyll>, ["< 3.0.0", ">= 1.0.0"])
      s.add_dependency(%q<sprockets>, ["~> 2.10"])
      s.add_dependency(%q<sass>, [">= 0"])
      s.add_dependency(%q<bundler>, ["~> 1.3"])
      s.add_dependency(%q<rake>, [">= 0"])
      s.add_dependency(%q<rspec>, [">= 0"])
      s.add_dependency(%q<compass>, [">= 0"])
      s.add_dependency(%q<bourbon>, [">= 0"])
      s.add_dependency(%q<neat>, [">= 0"])
      s.add_dependency(%q<bootstrap-sass>, [">= 0"])
      s.add_dependency(%q<sass>, ["~> 3.2.13"])
    end
  else
    s.add_dependency(%q<jekyll>, ["< 3.0.0", ">= 1.0.0"])
    s.add_dependency(%q<sprockets>, ["~> 2.10"])
    s.add_dependency(%q<sass>, [">= 0"])
    s.add_dependency(%q<bundler>, ["~> 1.3"])
    s.add_dependency(%q<rake>, [">= 0"])
    s.add_dependency(%q<rspec>, [">= 0"])
    s.add_dependency(%q<compass>, [">= 0"])
    s.add_dependency(%q<bourbon>, [">= 0"])
    s.add_dependency(%q<neat>, [">= 0"])
    s.add_dependency(%q<bootstrap-sass>, [">= 0"])
    s.add_dependency(%q<sass>, ["~> 3.2.13"])
  end
end
